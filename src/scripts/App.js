var EventManager = window.EventManager || $({});

var App = window.App || {

	init : function() {

		if ( Utils.isMSIE() ) {
			$('html').addClass('ie');
		}

		Menu.init();
		Lang.init();

		if ( $('body').hasClass('template__home') ) {
			Home.init();
		} else {
			Trail.init();
		}
	}
};

$(window).ready(function(){

	App.init();
});
