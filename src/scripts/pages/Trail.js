
var Trail = window.Trail || {

	$scrollOrigin : null,
	isTouchDevice : null,
	scrollMap : false,

	init : function() {

		this.isTouchDevice = Utils.is_touch_device() || Utils.isMSIE();
		this.initScroll();
		this.initLoad();

		if ( !this.isTouchDevice ) {
			TrailWaypoints.init();
			TrailMap.init();
		} else {
			TrailMap.initTouch();
		}
	},

	initLoad : function () {

		$('img[data-src]').each(function(i, src){
			$(src).attr('src', $(src).data('src'));
		});

		$('div[data-background], a[data-background]').each(function(i, src){
			$(src).css('background-image', 'url('+$(src).data('background')+')');
		});
	},

	initScroll : function () {

		this.scrollMap = !$('.block-trail-map__map').hasClass('no-scroll');

		this.$scrollOrigin = Utils.isFirefox() ? $('html')[0] : $('body')[0];
		$(document).on('scroll', Utils.debounce( $.proxy(this.onScroll, this), 8 ));
		$(window).on('resize', Utils.debounce( $.proxy(this.onResize, this), 10 ));
	},

	onScroll : function () {

		var ch = this.$scrollOrigin.clientHeight;
		var st = this.$scrollOrigin.scrollTop;
		var sh = this.$scrollOrigin.scrollHeight;

		if ( !this.isTouchDevice ) {
			TrailWaypoints.update( ch, st, sh );
			if ( this.scrollMap ) TrailMap.update( ch, st );
		}
	},

	onResize : function(){

		TrailMap.resize();
	}
};