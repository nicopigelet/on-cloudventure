
var Home = window.Home || {

	$scrollOrigin : null,
	isTouchDevice : null,
	isFlat : false,

	init : function() {

		this.isTouchDevice = Utils.is_touch_device() || Utils.isMSIE();
		
		this.$scrollOrigin = Utils.isFirefox() ? $('html')[0] : $('body')[0];
		this.$scrollTarget = $('.scroll-percentage');

		EventManager.on('VENTURE_LOADING', $.proxy(this.onLoading, this));
		EventManager.on('VENTURE_LOADED', $.proxy(this.onLoadComplete, this));
		EventManager.on('VENTURE_LAZY_LOADED', $.proxy(this.onLazyLoadComplete, this));
		EventManager.on('VENTURE_LOAD_PATH_MORPHED', $.proxy(this.onLoadPathMorphed, this));
		
		EventManager.on('VENTURE_SHOE_CHOSEN', $.proxy(this.onShoeChosen, this));
		EventManager.on('VENTURE_UPDATE_SHOE', $.proxy(this.onShoeUpdate, this));
		
		SVGPath.init();
		Loader.init();
	},

	onLoading : function( e, percent ) {
		SVGPath.onLoad( percent );
	},

	onLoadComplete : function() {

		console.log("on load complete");
		
		if ( !this.isTouchDevice ) {
			SVGLines.init();
			RainAnimation.init();
		}
		ChoiceHero.init();
		MiniChoice.init();
		
		$('body').addClass('template__choice');
		EventManager.trigger('VENTURE_HERO_IN', true);
	},

	onLazyLoadComplete : function(){
		
		// Set loaded in PHP Session
		$.ajax({ url : 'set_loaded.php' });
	},

	onShoeChosen : function() {

		this.initScroll();
		if ( !this.isTouchDevice ) HomeWaypoints.init();
		else this.updateShoe();
	},

	onShoeUpdate : function ( e, model ) {

		if ( !this.isTouchDevice ) HomeWaypoints.init();
		else this.updateShoe();
	},

	initScroll : function () {

		ScrollLine.init();
		$(document).on('scroll', Utils.debounce( $.proxy(this.onScroll, this), 8 ));
		$(window).on('resize', Utils.debounce( $.proxy(this.onResize, this), 100 ));
	},

	onResize : function () {

		this.repositionTooltip();
	},

	onScroll : function () {

		var ch = this.$scrollOrigin.clientHeight;
		var st = this.$scrollOrigin.scrollTop;
		var sh = this.$scrollOrigin.scrollHeight;

		MiniChoice.update( ch, st );
		ScrollLine.update( ch, st, sh );
		if ( !this.isTouchDevice ) HomeWaypoints.update( ch, st, sh );
	},

	onLoadPathMorphed : function () {

		var self = this;
		setTimeout( function () {
			ChoiceHero.initShoe();
			Slider.init();
			self.repositionTooltip();
		}, 1000 );
	},

	updateShoe : function () {
		
		// Set all feature to image max
		var model = $('body').data('model');
		$('body').addClass('has-touch');
		$('.block-feature.visible').each( function( i, feature ) {
			var 
				$feature = $(feature);

			$feature.find('img').attr('src', '/assets/img/' + model + '/' + model + '-sequence-' + (i+1) + '-' + $feature.data('max') + '.png' );
		} );

		// Reposition tooltips
		this.repositionTooltip();
	},

	repositionTooltip : function () {

		$('.block-feature__tooltip').each( function (i, tooltip) {

			var $tooltip = $(tooltip),
				$parent = $tooltip.parent(),
				parentWidth = $parent.width(),
				parentHeight = $parent.height(),
				refWidth = 880,
				refHeight = 422,
				top = parseInt($tooltip.data('top')),
				left = parseInt($tooltip.data('left')),
				newTop = top,
				newLeft = left;

			if ( $(window).innerWidth() < 880 ) {
				newTop = ( (parentHeight*top)/refHeight );
				newLeft = ( (parentWidth*left)/refWidth );
			}

			$tooltip.css({
				"top" : newTop,
				"left" : newLeft
			});

		} );
	}
};

