
var ChoiceHero = window.ChoiceHero || {

	timeoutAnimation : null,
	firstChoice : true,

	blockChoiceContent : null,
	blockHeroChoiceShoe : null,

	currentModel : "",
	currentClass : "",

	init : function() {

		this.initEvents();
	},

	initEvents : function () {

		this.blockChoiceContent = $('.block-hero');
		this.blockHeroChoiceShoe = $('.block-hero .choice__shoe');
		this.blockHeroChoiceShoe
			.on('mouseenter', $.proxy(this.onEnterShoe, this))
			.on('mouseleave', $.proxy(this.onLeaveShoe, this));
	},

	initShoe : function (){
		
		$('.block-hero').addClass('choice');
		this.onShoeChosen( null );
		$('.block-hero .choice__shoe, .block-choice-mini__shoe').on('click', $.proxy(this.onShoeChosen, this) );
	},

	onEnterShoe : function (e) {
		this.blockChoiceContent.addClass('over');
	},

	onLeaveShoe : function (e) {
		this.blockChoiceContent.removeClass('over');
	},

	onShoeChosen : function (e) {

		if ( e ) e.preventDefault();

		var 
			urlModel = (window.location.hash.split('/')[0]).replace('#', ''),
			defaultBlock;

		if ( e ) {
			defaultBlock = $(e.currentTarget);
		} else {

			if ( urlModel !== "" ) {
				defaultBlock = $('.block-hero .choice__shoe[data-model="'+urlModel+'"]');
			} else {
				defaultBlock = $('.block-hero .choice__shoe:eq(0)');
			}
		}

		var model = defaultBlock.length ? defaultBlock.data('model') : $('.block-hero .choice__shoe:eq(0)').data('model');
		
		var themeClass = 'theme-' + model;
		if ( model == this.currentModel ) {
			this.scrollToContent();
			return;
		}

		$('.block-feature').removeClass('visible');
		if ( this.currentClass !== "" ) $('.'+this.currentClass).removeClass('visible');
		$('.'+themeClass).addClass('visible');

		$('.template__loading').removeClass('template__loading');

		$('body')
			.data('model', model)
			.addClass(themeClass)
			.removeClass(this.currentClass);

		$('.block-choice-mini [data-model="'+this.currentModel+'"]').removeClass('selected');
		$('.block-choice-mini [data-model="'+model+'"]').addClass('selected');

		this.currentClass = themeClass;
		this.currentModel = model;
		
		if ( this.firstChoice ) {

			EventManager.trigger('VENTURE_SHOE_CHOSEN');
			this.firstChoice = false;

			if ( urlModel !== "" && urlModel !== "video" ) {
				this.scrollToContent( (window.location.hash.split('/')[1]) );
			}

		} else {

			EventManager.trigger('VENTURE_UPDATE_SHOE', model);
			this.scrollToContent();
		}
	},

	scrollToContent : function( feature ) {

		var blockFeaturePos = feature ? $('.block-feature__'+feature+'.visible').position().top : null;
		var scrollToPosition = blockFeaturePos || $('.block-hero').innerHeight();

		clearTimeout( this.timeoutAnimation );
		this.timeoutAnimation = setTimeout( function(){
			$('html, body').stop().animate({
				'scrollTop' : scrollToPosition
			}, (blockFeaturePos ? 0 : 650) );
		}, 100 );
	}
};