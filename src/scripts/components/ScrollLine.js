
var ScrollLine = window.ScrollLine || {

	$scrollTarget : null,

	init : function() {
		
		this.$scrollTarget = $('.scroll-percentage');
	},

	update : function ( ch, st, sh ) {

		if ( st < ch ) {
			this.$scrollTarget.css('width', '0%' );
			return;
		}

		var scrollPercentage = 100 * (st - ch) / (sh - ch - ch);
		this.$scrollTarget.css('width', scrollPercentage + '%' );
	}
};