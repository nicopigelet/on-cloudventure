
var Lang = window.Lang || {
	
	init : function() {

		$('.lang a').on('click', function(e){
			e.preventDefault();
			$.ajax( {
				url : '/set_lang.php',
				data : {
					'lang': $(this).data('lang') 
				},
				success : function() {
					window.location.reload(false);
				}
			});
		});
	}
};