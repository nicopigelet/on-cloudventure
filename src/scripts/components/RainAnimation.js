
  // RAIN ANIMATION

  /*
    gravity : 0.4,
    wind : 0.015,
    rain_chance : 0.3,
  */  
  var Config = {
    run : false,

    gravity : 0.25,
    wind : 0.03,
    rain_chance : 0.6,
    drop_size : 1,

    drop_colour : 'rgba(255,255,255,0.5)'
  };

  window.requestAnimFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function(callback) {
          window.setTimeout(callback, 1000 / 60);
      };

  //--------------------------------------------

  var Vector = function(x, y) {

    this.x = x || 0;
    this.y = y || 0;
  };

  Vector.prototype.add = function(v) {

    if (v.x != null && v.y != null) {

      this.x += v.x;
      this.y += v.y;

    } else {

      this.x += v;
      this.y += v;
    }

    return this;
  };

  Vector.prototype.copy = function() {

    return new Vector(this.x, this.y);
  };

  //--------------------------------------------

  var Rain = function() {

    this.pos = new Vector(Math.random() * RainAnimation.canvas.width, -50);
    this.prev = this.pos;

    this.vel = new Vector();
  };

  Rain.prototype.update = function() {

    this.prev = this.pos.copy();

    this.vel.y += Config.gravity;
    this.vel.x += Config.wind;

    this.pos.add(this.vel);
  };

  Rain.prototype.draw = function() {

    RainAnimation.ctx.beginPath();
    RainAnimation.ctx.moveTo(this.pos.x, this.pos.y);
    RainAnimation.ctx.lineTo(this.prev.x, this.prev.y);
    RainAnimation.ctx.stroke();
  };

  //--------------------------------------------

  var Drop = function(x, y) {

    var dist = Math.random() * 7;
    var angle = Math.PI + Math.random() * Math.PI;

    this.pos = new Vector(x, y);

    this.vel = new Vector(
      Math.cos(angle) * dist,
      Math.sin(angle) * dist
      );
  };

  Drop.prototype.update = function() {

    this.vel.y += Config.gravity;

    this.vel.x *= 0.95;
    this.vel.y *= 0.95;

    this.pos.add(this.vel);
  };

  Drop.prototype.draw = function() {

    RainAnimation.ctx.beginPath();
    RainAnimation.ctx.arc(this.pos.x, this.pos.y, 1, 0, Math.PI * 2);
    RainAnimation.ctx.fill();
  };

  //--------------------------------------------

  var RainAnimation = {

    canvas : null,
    rain : [],
    drops : [],

    init : function () {

      EventManager.on('VENTURE_FEATURE_UPDATE', $.proxy( this.onUpdate, this ));
    },

    onUpdate : function (e, shoe, feature, position) {

      if ( feature !== 1 ) return;

      RainAnimation.canvas = $('.canvas-rain')[0];

      switch ( position ) {
        case 1:
          RainAnimation.start();
        break;

        case 0:
        case 2:
          RainAnimation.stop();
        break;
        default:
          RainAnimation.stop();
        break;
      }
    },

    update : function() {

      RainAnimation.ctx.clearRect(0, 0, RainAnimation.canvas.width, RainAnimation.canvas.height);

      var i = RainAnimation.rain.length;
      while (i--) {

        var raindrop = RainAnimation.rain[i];

        raindrop.update();

        if (raindrop.pos.y >= RainAnimation.canvas.height) {

          var n = Math.round(4 + Math.random() * 4);

          while (n--)
          RainAnimation.drops.push(new Drop(raindrop.pos.x, RainAnimation.canvas.height));

          RainAnimation.rain.splice(i, 1);
        }

        raindrop.draw();
      }

      var j = RainAnimation.drops.length;
      while (j--) {

        var drop = RainAnimation.drops[j];
        drop.update();
        drop.draw();

        if (drop.pos.y > RainAnimation.canvas.height) RainAnimation.drops.splice(j, 1);
      }

      if (Math.random() < Config.rain_chance) RainAnimation.rain.push(new Rain());

      if ( Config.run ) window.requestAnimFrame( RainAnimation.update );
      else RainAnimation.ctx.clearRect(0, 0, RainAnimation.canvas.width, RainAnimation.canvas.height);
    },

    start : function (  ) {
      
      Config.run = true;
      
      RainAnimation.ctx = RainAnimation.canvas.getContext('2d');
      RainAnimation.canvas.width = 1600;
      RainAnimation.canvas.height = 720;

      RainAnimation.ctx.lineWidth = Config.drop_size;
      RainAnimation.ctx.strokeStyle = Config.drop_colour;
      RainAnimation.ctx.fillStyle = Config.drop_colour;

      RainAnimation.update();
    },

    stop : function () {

      Config.run = false;
    }

  };

