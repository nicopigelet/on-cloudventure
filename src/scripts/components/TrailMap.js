var TrailMap = window.TrailMap || {

	red: '#d75e5e',
	grey: '#777777',

	$dom : null,
	topTrigger : null,

	trailPath : null,
	overlayPath : null,
	maxLen : null,
	maxMiles : null,
	noScroll : null,

	endMarker : null,
	endIcon : null,
	endIconRed : null,

	trailCoordinates : [],
	isMapReady : false,

	init : function () {

		this.$dom = $('.block-trail-map__map');
		this.$domContainer = $('.block-trail-map__map__container');
		this.maxMiles = $('.block-trail-map__map .mile').length;

		this.noScroll = this.$dom.hasClass('no-scroll');
		
		this.resize();
	},

	initTouch : function() {

		this.$dom = $('.block-trail-map__map');
		this.$domContainer = $('.block-trail-map__map__container');

		this.noScroll = this.$dom.hasClass('no-scroll');
		this.$dom.addClass('has-touch');
	},

	initMap : function () {

		var styles = [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}];

		this.map = new google.maps.Map( $('.block-trail-map__map__path')[0], {
			center: {
				lat: 47.249385, 
				lng: 9.3432343
			},
			scrollwheel: false,
			draggable: false,
			zoom: 6,
			disableDefaultUI: true,
			mapTypeId: google.maps.MapTypeId.TERRAIN,
			styles: styles
		});

		this.resize();
		this.getGPX();
	},

	getGPX : function () {

		console.log( 'get gpx', this.$dom.data('file') );

		$.get({
			url : '../../data/trails/' + this.$dom.data('file'),
			success : $.proxy(this.onGPXLoaded, this)
		});
	},

	onGPXLoaded : function ( gpx ) {

		console.log( 'gpx loaded', gpx );

		// BUILD Path
		// ZOOM Map to bounds

		var 
			$point,
			pointCoords, lat, lng,
			self = this,
			bounds = new google.maps.LatLngBounds();

		$(gpx).find('trk').find('trkpt').each( function( i, point ){

			$point = $(point);
			lat = parseFloat($point.attr('lat'));
			lng = parseFloat($point.attr('lon'));

			pointCoords = {
				lat : lat,
				lng : lng
			};
			self.trailCoordinates.push( pointCoords );

			bounds.extend( new google.maps.LatLng(lat, lng) );
		} );

		this.trailPath = new google.maps.Polyline({
			path: self.trailCoordinates,
			geodesic: true,
			strokeColor: this.grey,
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		this.maxLen = this.trailPath.getPath().length;

		this.overlayPath = new google.maps.Polyline({
			geodesic: true,
			strokeColor: this.red,
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		this.trailPath.setMap(this.map);
		this.overlayPath.setMap(this.map);

		this.map.fitBounds( bounds );

		this.isMapReady = true;

		this.addIcons();
		this.resize();

		if ( this.noScroll ) {
			this.overlayPath.setPath( this.trailCoordinates );
		}
	},

	addIcons : function () {
		
		var startIcon = {
			url: '../assets/img/skin/icon-start.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		var startMarker = new google.maps.Marker({
			position: {
				lat: this.trailCoordinates[0].lat,
				lng: this.trailCoordinates[0].lng
			},
			map: this.map,
			icon: startIcon
		});

		this.endIcon = {
			url: '../assets/img/skin/icon-end.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		this.endIconRed = {
			url: '../assets/img/skin/icon-end-red.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		var trailLenght = this.trailCoordinates.length;
		this.endMarker = new google.maps.Marker({
			position: {
				lat: this.trailCoordinates[trailLenght-1].lat,
				lng: this.trailCoordinates[trailLenght-1].lng
			},
			map: this.map,
			// icon: this.endIcon, 
			icon: this.endIconRed
		});

	},

	drawPath : function ( length ) {

		var visibleCoordinates = this.trailCoordinates.slice( 0, length );
		this.overlayPath.setPath( visibleCoordinates );

		if ( length >= this.trailCoordinates.length - 10 ) {
			this.endMarker.setIcon( this.endIconRed );
		} else {
			this.endMarker.setIcon( this.endIcon );
		}
	},

	updateMiles : function ( mile ) {

		$('.mile.visible').removeClass('visible');
		for ( var i = 0; i < mile; i++ ) {
			$('.mile:eq('+i+')').addClass('visible');
		}
	},

	update : function ( ch, st ) {

		var 
			mapHeight = 3000,
			maxScrollTop = this.topTrigger + mapHeight - ch;

		if ( st > this.topTrigger && st < maxScrollTop ) {

			this.$domContainer
				.removeClass('is-bottomed')
				.addClass('is-fixed');

			if ( this.isMapReady ) {
				this.drawPath( Math.ceil(this.maxLen * ( st - this.topTrigger ) / (mapHeight - ch - 100)) );
				this.updateMiles( Math.ceil(this.maxMiles * ( st - this.topTrigger ) / (mapHeight - ch - 100)) );
			}

		} else if ( st >= maxScrollTop ) {

			this.$domContainer
				.removeClass('is-fixed')
				.addClass('is-bottomed');

			if ( this.isMapReady ) this.overlayPath.setPath( this.trailCoordinates );

		} else {

			this.$domContainer
				.removeClass('is-bottomed')
				.removeClass('is-fixed');

			if ( this.isMapReady ) {
				this.overlayPath.setPath( [] );
				$('.mile.visible').removeClass('visible');
			}
		}
	},

	resize : function() {

		var 
			windowH = $(window).innerHeight(),
			windowW = $(window).innerWidth();

		if ( this.$domContainer ) this.$domContainer.css('height', windowH);
		if ( this.$dom ) this.topTrigger = this.$dom.position().top;

		if ( windowW > 690 && windowW < 1024 && $('.block-trail-map__map').hasClass('has-touch') ) {
			$('.block-trail-map__map__miles__scroller__inside').css('width', $('.block-trail-map__map .mile').length * 142);
		}
	}
};