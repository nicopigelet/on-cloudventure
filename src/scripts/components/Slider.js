
var Slider = window.Slider || {

	currentSlick : null,

	init : function() {

		EventManager.on('VENTURE_UPDATE_SHOE', $.proxy( this.resetSlick, this ));
		this.initSlick();
	},

	resetSlick : function () {
		
		this.destroySlick();
		this.initSlick();
	},

	initSlick : function() {

		this.currentSlick = $('.visible .slider-tooltips');
		this.currentSlick.slick({
			dots : true
		});
	},

	destroySlick : function () {

		if ( this.currentSlick ) this.currentSlick.slick('unslick');
	}
};