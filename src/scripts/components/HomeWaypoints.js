
var HomeWaypoints = window.HomeWaypoints || {

	els : [],
	img : [],
	imgTooltips : [],
	top : [],
	imgHeight : [],
	maxImg : [],

	elWatch : null,
	elWatchGuy : null,
	elWatchBackground : null,
	elWatchTop : null,
	elWatchHeight : null,

	elFind : null,
	elFindBackground : null,
	elFindTop : null,
	elFindHeight : null,

	features : null,
	currentIndex : 0,

	activeGrip : false,
	activeUpper : false,
	activeCushion : false,
	activeWatch : null,
	activeFind : null,

	windowH : null,
	windowW : null,

	shoe : "",

	init : function() {

		var self = this;
		self.setShoe( $('body').data('model') );

		self.img = [];
		self.imgTooltips = [];
		self.top = [];
		self.imgHeight = [];
		self.maxImg = [];

		self.els = [
			$('.block-feature__grip.visible'),
			$('.block-feature__upper.visible'),
			$('.block-feature__cushioning.visible')
		];
		$( self.els ).each( function(i, el) {
			var 
				$el = $(el),
				$img = $el.find('.block-image img'),
				$imgTooltips = $el.find('.block-feature__tooltip');

			self.img.push( $img );
			self.imgTooltips.push( $imgTooltips );
			self.top.push( $el.position().top );
			
			var ratio = ( i == 1 ) ? 1 : 1.2;
			self.imgHeight.push( ($img.height() * ratio) );
			self.maxImg.push( $el.data('max') );
		} );
		
		self.elWatch = $('.block-watch');
		self.elWatchTop = self.elWatch.position().top;
		self.elWatchHeight = self.elWatch.height();
		self.elWatchGuy = $('.block-watch__guy');
		self.elWatchBackground = $('.block-watch__background');
		
		self.elFind = $('.block-find-venture.visible');
		self.elFindTop = self.elFind.position().top;
		self.elFindHeight = self.elFind.height();
		self.elFindBackground = self.elFind.find('.block-find-venture__background');

		self.features = $('.block-choice-mini__feature.visible');
		self.features.removeClass('selected');
		self.currentIndex = 0;
		self.features.eq(self.currentIndex).addClass('selected');

		self.windowH = window.innerHeight;
		self.windowW = window.innerWidth;

		Waypoint.destroyAll();

		var waypointHero = new Waypoint({
			element: $('.block-hero'),
			handler: function(direction) {
				EventManager.trigger( ( direction == "down" ) ? 'VENTURE_HERO_OUT' : 'VENTURE_HERO_IN');
			},
			offset: '-65%'
		});

		var inviewGrip = new Waypoint.Inview({
			element: self.els[0],
			enter: function ( direction ) {
				self.activeGrip = true;
				self.updateFeatureMenu(0);
			},
			exited: function() {
				self.activeGrip = false;
			}
		});

		var waypointGripIn = new Waypoint({
			element: self.els[0],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 0, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointGripOut = new Waypoint({
			element: self.els[0],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 0, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewUpper = new Waypoint.Inview({
			element: self.els[1],
			enter: function() {
				self.activeUpper = true;
				self.updateFeatureMenu(1);
			},
			exited: function() {
				self.activeUpper = false;
			}
		});

		var waypointUpperIn = new Waypoint({
			element: self.els[1],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 1, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointUpperOut = new Waypoint({
			element: self.els[1],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 1, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewCushion = new Waypoint.Inview({
			element: self.els[2],
			enter: function() {
				self.activeCushion = true;
				self.updateFeatureMenu(2);
			},
			exited: function() {
				self.activeCushion = false;
			}
		});

		var waypointCushionIn = new Waypoint({
			element: self.els[2],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 2, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointCushionOut = new Waypoint({
			element: self.els[2],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 2, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewWatch = new Waypoint.Inview({
			element: self.elWatch,
			enter: function() {
				self.activeWatch = true;
			},
			exited: function() {
				self.activeWatch = false;
			}
		});

		var inviewFind = new Waypoint.Inview({
			element: self.elFind,
			enter: function() {
				self.activeFind = true;
			},
			exited: function() {
				self.activeFind = false;
			}
		});
	},

	updateFeatureMenu : function ( index ) {

		this.features.eq(this.currentIndex).removeClass('selected');
		this.features.eq(index).addClass('selected');
		this.currentIndex = index;

		var model = $('body').data('model');
		if ( model !== '' ) {
			var selectedfeature = this.els[index].data('feature') || null;
			var hash = model + (selectedfeature ? ('/'+ selectedfeature) : '' );
			window.location.hash = hash;
		}
	},

	setShoe : function( shoe ) {

		this.shoe = shoe;
	},

	update : function ( clientHeight, scrollTop, scrollHeight ) {

		if ( this.activeWatch ){
			this.updateWatch( scrollTop, clientHeight );
		}

		if ( this.activeFind ){
			this.updateFind( scrollTop, clientHeight );
		}

		if ( !this.activeGrip && !this.activeUpper && !this.activeCushion ) return;

		if ( this.activeGrip ){
			this.update360( 0, scrollTop, clientHeight );
		}

		if ( this.activeUpper ){
			this.update360( 1, scrollTop, clientHeight );
		}

		if ( this.activeCushion ){
			this.update360( 2, scrollTop, clientHeight );
		}
	},

	updateWatch : function ( scrollTop, clientHeight ) {

		var translateValGuy = Utils.map( scrollTop, this.elWatchTop - clientHeight, this.elWatchTop+this.elWatchHeight, 280, 20 );
		this.elWatchGuy.css('transform', 'translateY('+translateValGuy+'px)');

		if ( this.windowW < 1024 ) return;
		this.elWatchBackground.css('transform', 'translateY('+(-translateValGuy+20)+'px)');
	},

	updateFind : function ( scrollTop, clientHeight ) {

		var translateVal = Utils.map( scrollTop, this.elFindTop - clientHeight, this.elFindTop+this.elFindHeight, 5, -30 );
		this.elFindBackground.css('transform', 'translateY('+translateVal+'%)');
	},

	update360 : function ( index, scrollTop, clientHeight ) {

		var 
			$img = this.img[index],
			$imgTooltips = this.imgTooltips[index],
			top = this.top[index], 
			imgHeight = this.imgHeight[index], 
			maxImg = this.maxImg[index],
			min = top - clientHeight + imgHeight,
			max = min + 280,
			percent = 0,
			shoe = this.shoe,
			indexSequence = index + 1;

		if ( scrollTop > min && scrollTop < max ) {
			
			percent = Math.ceil( maxImg * (scrollTop - min) / (max - min) );
			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-' + percent + '.png' );
			$imgTooltips.removeClass('visible');

		} else if ( scrollTop <= min ) {

			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-1.png' );

		} else if ( scrollTop >= max ) {
			
			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-' + maxImg + '.png' );
			$imgTooltips.addClass('visible');
		}
	}
};
