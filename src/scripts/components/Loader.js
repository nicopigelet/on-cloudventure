var Loader = window.Loader || {

	loading : null,

	maxImg : 0,
	imgLoaded : 0,
	$percentage : null,
	$title1 : null,
	$title2 : null,

	maxImgLazy : 0,
	imgLazyLoaded : 0,
	
	init : function() {

		this.$title1 = $('.block-hero .loading__content .block-title__loading-1');
		this.$title2 = $('.block-hero .loading__content .block-title__loading-2');
		this.$percentage = $('.loading-value');

		this.loading = {
			load : 0,
			update : 0
		};

		// if ( (window.imgToLoad && window.imgToLoad.length > 0) /*&& window.hasLoaded === ''*/ ) {

		var imgToLoad = $(':not(iframe)[data-src], [data-background], [data-choice]');

		if ( imgToLoad.length > 0 && window.hasLoaded === '' ) {
		
			this.maxImg = imgToLoad.length;
			var self = this;

			$(imgToLoad).each( function( i, src ) {

				var $src = $(src);
				var img = new Image();
				img.alt = $src.data('src') ? 'src' : ( $src.data('background') ? 'background' : 'choice' );
				img.onload = $.proxy(self.onImgLoaded, self);
				img.onerror = $.proxy(self.onImgLoaded, self);
				img.src = $src.data('src') || $src.data('background') || $src.data('choice');
			} );

			this.rafUpdate = true;
			requestAnimationFrame( $.proxy(this.updateLoad, this) );

		} else {

			$('img[data-src]').each(function(i, src){
				$(src).attr('src', $(src).data('src'));
			});

			$('div[data-background], a[data-background]').each(function(i, src){
				$(src).css('background-image', 'url('+$(src).data('background')+')');
			});

			EventManager.trigger('VENTURE_LOADING', 100);
			this.onLoadComplete();
		}
	},

	updateLoad : function() {

		if ( this.loading.update >= 100 ) {
			this.onLoadComplete(); 
			return;
		}

		if ( this.loading.update <= this.loading.load ) {
			this.loading.update += ( window.DEBUG ) ? 100 : 0.4;
		}
		this.onLoad();
		
		requestAnimationFrame( $.proxy(this.updateLoad, this) );
	},

	onImgLoaded : function (e) {

		var 
			type = e.currentTarget.alt,
			src = $(e.currentTarget).attr('src');

		if ( type == "src" ) {
			$('[data-'+type+'="'+src+'"]').attr('src', src);
		} else if ( type == "background" ) {
			$('[data-'+type+'="'+src+'"]').css('background-image', 'url('+src+')');
		}

		this.imgLoaded++;
		this.loading.load = this.imgLoaded * 100 / this.maxImg;
	},

	onLoad : function () {

		var percent = (this.loading.update >> 0);
		
		if ( percent >= 2 && percent < 50 ) {
			this.$title1.addClass('visible');
		} else {
			this.$title1.removeClass('visible');
		}
		if ( percent >= 55 ) {
			this.$title2.addClass('visible');
		}

		this.$percentage.text( percent+'%' );
		EventManager.trigger('VENTURE_LOADING', percent);
	},

	onLoadComplete : function () {

		EventManager.trigger('VENTURE_LOADED');
		this.initLazyLoad();
	},

	initLazyLoad : function () {

		var self = this;

		self.maxImgLazy = window.imgToLazyLoad.length;
		$(window.imgToLazyLoad).each( function( i, src ) {

			var img = new Image();
			img.onload = $.proxy(self.onImgLazyLoaded, self);
			img.onerror = $.proxy(self.onImgLazyLoaded, self);
			img.src = src;
		} );
	},

	onImgLazyLoaded : function () {
		this.imgLazyLoaded++;
		if ( this.imgLazyLoaded >= this.maxImgLazy ) {
			console.log('lazy load complete');
			EventManager.trigger('VENTURE_LAZY_LOADED');
		}
	}

};