
var TrailWaypoints = window.TrailWaypoints || {

	elHero : null,
	elBackground : null,
	activeHero : null,

	init : function() {

		var self = this;
		
		self.elHero = $('.block-trail-hero');
		self.elBackground = $('.block-trail-hero__background');

		Waypoint.destroyAll();

		var inviewHero = new Waypoint.Inview({
			element: self.elHero,
			enter: function() {
				self.activeHero = true;
			},
			exited: function() {
				self.activeHero = false;
			}
		});
	},

	update : function ( clientHeight, scrollTop, scrollHeight ) {

		if ( !this.activeHero ) return;
		this.updateHero( scrollTop, clientHeight );
	},

	updateHero : function ( scrollTop, clientHeight ) {

		var translateValue = Utils.map( scrollTop, 0, clientHeight, 0, (0.2*clientHeight) );
		this.elBackground.css('transform', 'translateY('+translateValue+'px)');
	}
};