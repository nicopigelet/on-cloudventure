
var Menu = window.Menu || {

	timeoutOverlay : null,
	previousHash : "",
	
	init : function() {

		this.initFixHeaderMaster();
		this.initMasterHeaderButton();
		this.initWatchButton();

		this.initMobileMenu();

		this.initEscKey();
	},

	initMobileMenu : function () {

		$('.btn-open-menu').on('click', function(e){
			e.preventDefault();
			$('.header').addClass('menu-open');
		});

		$('.btn-close-menu').on('click', function(e){
			e.preventDefault();
			$('.header').removeClass('menu-open');
		});
	},

	initFixHeaderMaster : function () {

		$('.header-master a').each( function(i, link) {

			var 
				$link = $(link),
				href = $link.attr('href');

			if ( href.match( /^\// ) ) {
				$link.attr('href', 'http://on-running.com'+href);
			}
		});

		$('.logo').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = "/";
		});

		$('.btn-take-trail').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = $('.btn-take-trail').attr('href');
		});
		
		$('.block-trail-take__trail').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = $(this).attr('href');
		});
	},

	initEscKey : function () {
		var self = this;
		$('body').on('keyup', function(e) {
			if ( e.keyCode == '27' && $('.overlay-watch').hasClass('visible') ) self.toggleVideoOverlay(null);
			if ( e.keyCode == '27' && $('.header-master').hasClass('visible') ) self.toggleHeaderMaster(null);
		});
	},

	initMasterHeaderButton : function () {
		
		$('[data-action="master"]').on('click', $.proxy(this.toggleHeaderMaster, this));
	},

	toggleHeaderMaster : function (e) {

		if ( e ) e.preventDefault();

		$('body').toggleClass('header-master__open');

		if ( $(e.target).hasClass('btn-open-master') ) {
			$('html, body').stop(true, true).animate({
				scrollTop : 0
			}, 200);
		}
	},

	initWatchButton : function () {

		var urlModel = (window.location.hash.split('/')[0]).replace('#', '');
		if ( urlModel == "video" ) this.toggleVideoOverlay();
		
		$('[data-popin="watch"]').on('click', $.proxy(this.toggleVideoOverlay, this));
	},

	toggleVideoOverlay : function (e) {

		if (e) e.preventDefault();

		clearTimeout( this.timeoutOverlay );

		var 
			$overlay = $('.overlay-watch'),
			$frame = $overlay.find('iframe');

		if ( $overlay.hasClass('visible') ) {
			
			$frame.attr('src', '');
			$overlay.removeClass('visible');
			if ( this.previousHash ) window.location.hash = this.previousHash;

		} else {
			$overlay.addClass('visible');
			this.previousHash = window.location.hash;
			window.location.hash = "video";

			this.timeoutOverlay = setTimeout( function(){
				$frame.attr('src', $frame.data('src'));
			}, 200 );
		}
	}
};