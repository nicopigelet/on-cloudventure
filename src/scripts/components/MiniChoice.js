
var MiniChoice = window.MiniChoice || {

	$dom : null,
	$choice : null,
	$background : null,

	init : function () {

		this.$dom = $('.block-choice-mini, .background-choice');
		this.$choice = $('.block-choice-mini');
		this.$background = $('.background-choice');

		this.initRollover();
		this.initClick();
	},

	initClick : function() {

		this.$choice.find('.block-choice-mini__shoe__link-feature').on('click', function(e){

			e.preventDefault();

			var $feature = $('.block-feature.visible[data-feature='+$(this).data('feature')+']');
			$('html, body').stop(true,true).animate({
				scrollTop : $feature.position().top
			}, 600);

		});
	},

	initRollover : function() {

		var self = this;
		this.$choice.on('mouseenter', function(){
			self.$background.addClass('is-visible');
		});

		this.$choice.on('mouseleave', function(){
			self.$background.removeClass('is-visible');
		});
	},

	update : function (ch, st) {

		if ( st > ch ) {
			this.$dom.addClass('is-fixed');
		} else {
			this.$dom.removeClass('is-fixed');
		}
	}
};