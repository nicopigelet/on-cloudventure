# On Cloudventure  

Front-end: @nicopigelet  

Client: <http://on-running.com/>  

Local: <http://cloudventure.localhost/>  
Live: <http://cloudventure.on-running.com/>  

## System Requirements (Dev)  

- Node.js (<http://nodejs.org/>)
- Gulp (<http://gulpjs.com/>)

## JS Libraries / Plugins  

- jQuery *(v2.1.4)*  
  <http://jquery.com>

- PreloadJS
