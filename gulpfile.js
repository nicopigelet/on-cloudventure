
var 
	gulp = require('gulp'),

	sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),

    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    
	notify = require('gulp-notify');


// STYLE TASKS
gulp.task('styles', function() {
	return sass('src/styles/main.scss', { style: 'expanded' })
		.pipe(autoprefixer({
            browser : ['last 2 version', 'ie 9', 'ie 8', 'safari 8']
        }))
		.pipe(gulp.dest('static/assets/css'))
		.pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-build', function(){
	return sass('src/styles/main.scss', { style: 'expanded' })
		.pipe(autoprefixer({
            browser : ['last 2 version', 'ie 9', 'ie 8', 'safari 8']
        }))
		.pipe(gulp.dest('static/assets/css'))
		.pipe(rename({suffix: '.min'}))
		.pipe(cssnano())
		.pipe(gulp.dest('static/assets/css'))
		.pipe(notify({ message: 'Styles task complete' }));
});


// JS TASKS
gulp.task('scripts', function() {
  return gulp.src(['src/scripts/**/*.js', '!src/scripts/lib/*.js'])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('static/assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('scripts-lib', function() {
  return gulp.src(['src/scripts/lib/*.js'])
    .pipe(concat('lib.js'))
    .pipe(gulp.dest('static/assets/js/lib'))
    .pipe(notify({ message: 'Scripts task LIB complete' }));
});

gulp.task('scripts-build', function() {
  return gulp.src('src/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('static/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('static/assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});


// WATCH TASKS
gulp.task('watch', function() {

	// Watch .scss files
	gulp.watch('src/styles/**/*.scss', ['styles']);

	// Watch .js files
	gulp.watch('src/scripts/**/*.js', ['scripts']);
  
});

// BUILD TASKS
gulp.task('build', function() {
	gulp.start('styles-build', 'scripts-build');
});
