
<!DOCTYPE html>
<html>
<head>
  <title></title>

  <style type="text/css" media="screen">
    
    .wrapper {

      display: block;
      width: 100%;
      height: 100%;

      position: absolute;
      top: 0;
      left: 0;

      background: url(./assets/img/backgrounds/feature-grip_waterproof.jpg) no-repeat center center;
      background-size: cover;
    }

    canvas {
      position: absolute;
      top: 0;
      left: 0;
      background: none;

      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>

<div class="wrapper">
  <canvas id="c"></canvas>
</div>

<script type="text/javascript" src="./assets/js/lib/dat.gui.min.js"></script>
<script type="text/javascript" charset="utf-8">

  // RAIN ANIMATION

  var rain = [], drops = [];

  // var gravity = 0.2;
  // var wind = 0.015;
  // var rain_chance = 0.2;

  var Config = {
    run : false,

    gravity : 0.4,
    wind : 0.015,
    rain_chance : 0.3,
    drop_size : 1,

    drop_colour : 'rgba(255,255,255,0.5)'
  };

  window.requestAnimFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function(callback) {
          window.setTimeout(callback, 1000 / 60);
      };

  var canvas = document.getElementById('c');
  var ctx = canvas.getContext('2d');
  canvas.width = 1600;
  canvas.height = 720;

  //--------------------------------------------

  var Vector = function(x, y) {

    this.x = x || 0;
    this.y = y || 0;
  };

  Vector.prototype.add = function(v) {

    if (v.x != null && v.y != null) {

      this.x += v.x;
      this.y += v.y;

    } else {

      this.x += v;
      this.y += v;
    }

    return this;
  };

  Vector.prototype.copy = function() {

    return new Vector(this.x, this.y);
  };

  //--------------------------------------------

  var Rain = function() {

    this.pos = new Vector(Math.random() * canvas.width, -50);
    this.prev = this.pos;

    this.vel = new Vector();
  };

  Rain.prototype.update = function() {

    this.prev = this.pos.copy();

    this.vel.y += Config.gravity;
    this.vel.x += Config.wind;

    this.pos.add(this.vel);
  };

  Rain.prototype.draw = function() {

    ctx.beginPath();
    ctx.moveTo(this.pos.x, this.pos.y);
    ctx.lineTo(this.prev.x, this.prev.y);
    ctx.stroke();
  };

  //--------------------------------------------

  var Drop = function(x, y) {

    var dist = Math.random() * 7;
    var angle = Math.PI + Math.random() * Math.PI;

    this.pos = new Vector(x, y);

    this.vel = new Vector(
      Math.cos(angle) * dist,
      Math.sin(angle) * dist
      );
  };

  Drop.prototype.update = function() {

    this.vel.y += Config.gravity;

    this.vel.x *= 0.95;
    this.vel.y *= 0.95;

    this.pos.add(this.vel);
  };

  Drop.prototype.draw = function() {

    ctx.beginPath();
    ctx.arc(this.pos.x, this.pos.y, 1, 0, Math.PI * 2);
    ctx.fill();
  };

  //--------------------------------------------

  var RainAnimation = {

    update : function() {

      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.lineWidth = Config.drop_size;

      var i = rain.length;
      while (i--) {

        var raindrop = rain[i];

        raindrop.update();

        if (raindrop.pos.y >= canvas.height) {

          var n = Math.round(4 + Math.random() * 4);

          while (n--)
          drops.push(new Drop(raindrop.pos.x, canvas.height));

          rain.splice(i, 1);
        }

        raindrop.draw();
      }

      var i = drops.length;
      while (i--) {

        var drop = drops[i];
        drop.update();
        drop.draw();

        if (drop.pos.y > canvas.height) drops.splice(i, 1);
      }

      if (Math.random() < Config.rain_chance) rain.push(new Rain());

      if ( Config.run ) requestAnimFrame( RainAnimation.update );
    },

    start : function () {

      Config.run = true;

      ctx.lineWidth = Config.drop_size;
      // ctx.strokeStyle = 'rgba(60,135,235,1)';
      // ctx.fillStyle = 'rgba(60,135,235,1)';
      ctx.strokeStyle = Config.drop_colour;
      ctx.fillStyle = Config.drop_colour;

      RainAnimation.update();
    },

    stop : function () {

      Config.run = false;
    }

  }

  RainAnimation.start();


  // DAT.GUI
  var gui = new dat.GUI();
  gui.add(Config, 'gravity', 0, 1, 0.05);
  gui.add(Config, 'wind', 0, 0.2, 0.005);
  gui.add(Config, 'rain_chance', 0, 1, 0.1);
  gui.add(Config, 'drop_size', 1, 4, 1);

</script>


</body>
</html>