<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	function getLanguageByGeoIP() {

		$cookieLang = !empty( $_COOKIE['venture-language'] ) ? $_COOKIE['venture-language'] : null;
		if ( $cookieLang ) {
			$language = $cookieLang;
			return $language;
			die();
		}

		$language = 'en';
		if ( !empty($_SERVER["HTTP_CF_IPCOUNTRY"]) ) {

			switch ( $_SERVER["HTTP_CF_IPCOUNTRY"] ) {

				case "CH" :
				case "DE" : 
					$language = "de";
					break;
					
				case "ES" : 
					$language = "es";
					break;
					
				case "BE" :
				case "FR" : 
					$language = "fr";
					break;
					
				case "IT" : 
					$language = "it";
					break;
					
				case "JP" : 
					$language = "jp";
					break;
					
				case "EN" : 
				default :
					$language = "en";
					break;
			}

		}

		/*
		
		$language = 'en';
		if ( !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ) {

			$httpLang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

			if ( preg_match('/de/', $httpLang) ) {
				$language = 'de';
			}
			if ( preg_match('/es/', $httpLang) ) {
				$language = 'es';
			}
			if ( preg_match('/fr/', $httpLang) ) {
				$language = 'fr';
			}
			if ( preg_match('/it/', $httpLang) ) {
				$language = 'it';
			}
			if ( preg_match('/jp|ja/', $httpLang) ) {
				$language = 'jp';
			}
		}
		*/

		/*
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		// $ip = '213.3.51.86';
		// echo ' - IP : ' . $ip;

		if ( $ip == "127.0.0.1" ) {

			setcookie('venture-language', 'en');
			return "en";
			die();
		}

		// echo ' - get geo ip';
		$geoIPContent = file_get_contents('http://freegeoip.net/json/' . $ip);
		$parsedJsonGeo  = json_decode($geoIPContent);
		$language = "";

		switch ( $parsedJsonGeo->country_code ) {

			case "CH" :
			case "DE" : 
				$language = "de";
				break;
				
			case "ES" : 
				$language = "es";
				break;
				
			case "BE" :
			case "FR" : 
				$language = "fr";
				break;
				
			case "IT" : 
				$language = "it";
				break;
				
			case "JP" : 
				$language = "jp";
				break;
				
			case "EN" : 
			default :
				$language = "en";
				break;
		}
		*/

		// echo ' - set cookie : '.$language;
		setcookie('venture-language', $language);
		return $language;
	}

?>