
<h2>Title H2 - <span class="blue">welcome to<br>the swiss alps</span></h2>

<br><br>

<h3>Title H3 - Cloudventure midtop</h3>

<br><br>

<h4 class="text">subtitle H4 - Trail performance</h4>
<div class="text">subtitle div - Trail performance</div>

<br><br>	

<div class="tagline">Tagline - Swiss Engineering</div>

<br><br>

<h2 class="extra">Born in the<br>Swiss Alps</h2>

<br><br>

<a href="#" class="button">
	<span class="label">Watch Film</span>
	<span class="background"></span>
</a>
