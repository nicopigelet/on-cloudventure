<?php

$cookieLang = isset( $_COOKIE['venture-language'] ) ? $_COOKIE['venture-language'] : "en";
$language = 'en-us';
switch ( $cookieLang ) {
    case "de" : 
        $language = "de-de";
        break;
        
    case "es" : 
        $language = "es-es";
        break;
        
    case "fr" : 
        $language = "fr-fr";
        break;
        
    case "it" : 
        $language = "it-it";
        break;
        
    case "jp" : 
        $language = "ja-jp";
        break;
        
    case "en" : 
    default :
        $language = "en-us";
        break;
}

$url = 'http://www.on-running.com/'.$language.'/html_snippet/header';
$file = 'cache/header-master.'.$language.'.php';
// $data = file_get_contents($url);
$data = get_content($file, $url);

echo $data;

?>
<a href="#" class="quit-background" data-action="master"></a>

<?php

/* gets the contents of a file if it exists, otherwise grabs and caches */
function get_content($file,$url,$hours = 24) {
    
    //vars
    $current_time = time(); 
    $expire_time = $hours * 60 * 60; 
    $file_time = @filemtime($file);

    //decisions, decisions
    if(file_exists($file) && ($current_time - $expire_time < $file_time)) {

        // echo 'returning from cached file';
        return file_get_contents($file);
    }
    else {
        $content = file_get_contents($url);
        $content.= '<!-- cached:  '.time().' -->';
        @file_put_contents($file,$content);

        // echo 'retrieved fresh from '.$url.':: '.$content;
        return $content;
    }
}

?>
