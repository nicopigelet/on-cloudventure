<?php
	session_start();

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	date_default_timezone_set('Europe/Zurich');

	$_SESSION['imgToLoad'] = array('/assets/img/skin/logo-on.png');
	$_SESSION['imgToLazyLoad'] = array();

	include 'get_geoip.php';
	$language = getLanguageByGeoIP();
	$trailLang = ($language == "de") ? $language : "en";

	$json_home_url = "data/".$language."/home.json";
	$json_texts_url = "data/".$language."/texts.json";
	$json_trails_url = "data/".$trailLang."/trails.json";

	$json_texts = file_get_contents($json_texts_url);
	$texts = json_decode($json_texts);
	
	$json_trails = file_get_contents($json_trails_url);
	$trails = json_decode($json_trails);

	$json_home = file_get_contents($json_home_url);
	$blocks = json_decode($json_home);

	$bodyClass = "template__home template__loading lang__".$language;

	include 'includes/layout/top.php';
	include 'includes/header.php'; ?>

		<div class="theme scroll-percentage"></div>

		<div class="wrapper">
			<?php 
			$choice = null;
			$features = array();
			$blockFeature = 0;

			$grip = array();
			$cushioning = array();

			foreach( $blocks as $i => $block ) {
				
				// Fill the mini choice block
				if ( $block->block == "choice" ) $choice = $block;

				if ( $block->block == "feature" ) {
					$blockFeature++;
					array_push($features, array( 
						"shoe" => $block->shoe,
						"slug" => $block->slug,
						"name" => $block->name
					));

					if ( $block->slug == "grip" /*&& count($grip) == 0*/ ) {
						$grip['title'] = $block->title;
						$grip['subtitle'] = $block->subtitle;
						$grip['description'] = $block->description;
						$grip['tooltips'] = $block->tooltips;
					}

					if ( $block->slug == "cushioning" && count($cushioning) == 0 ) {
						$cushioning['title'] = $block->title;
						$cushioning['subtitle'] = $block->subtitle;
						$cushioning['description'] = $block->description;
					}
				}

				include 'includes/blocks/block-'.$block->block.'.php';
			}

			include 'includes/footer.php';
			include 'includes/blocks/block-choice-mini.php'; 
			?>
		</div>
		
<?php 
	$hasMap = false;
	include 'includes/layout/bottom.php'; 
?>