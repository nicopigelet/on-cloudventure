<?php
	foreach ( $block->sequence as $img ) {
		array_push( $_SESSION['imgToLazyLoad'], '/assets/img/'.$block->folder.$img );
	}

	if ( $block->slug == 'grip' ) {

		$blockTitle = $grip['title'];
		$blockSubtitle = $grip['subtitle'];
		$blockDescription = $grip['description'];
		$blockTooltips = $grip['tooltips'];

	} else if ( $block->slug == 'cushioning' ) {

		$blockTitle = $cushioning['title'];
		$blockSubtitle = $cushioning['subtitle'];
		$blockDescription = $cushioning['description'];

	} else {

		$blockTitle = $block->title;
		$blockSubtitle = $block->subtitle;
		$blockDescription = $block->description;
	}
?>
<div class="block block-feature block-feature__<?php echo $block->slug; ?> theme-<?php echo implode(' theme-', $block->shoe); ?>" data-background="/assets/img/<?php echo $block->background; ?>" data-max="<?php echo count($block->sequence); ?>" data-feature="<?php echo $block->slug; ?>">
	
	<?php 
		if ( $block->shoe[0] == 'cloudventure-waterproof' && $block->slug == 'upper' ) {
			echo '<canvas id="c-'.$block->slug.'" class="canvas-rain"></canvas>';
		}
	?>

	<div class="block-image">
		<img data-src="/assets/img/<?php echo $block->folder.$block->image; ?>" alt="<?php echo $blockTitle. ' ' .$blockSubtitle; ?>">

		<?php if ( isset($block->tooltips) ) { 
			foreach ( $blockTooltips as $tooltip ) { ?>
			<div class="block-feature__tooltip <?php echo $tooltip->direction; ?>" style="top:<?php echo $tooltip->y; ?>;left:<?php echo $tooltip->x; ?>;" data-top="<?php echo $tooltip->y; ?>" data-left="<?php echo $tooltip->x; ?>">
				<div class="block-feature__tooltip__plus"></div>
				<div class="block-feature__tooltip__image">
					<div class="block-feature__tooltip__bg" data-background="<?php echo '/assets/img/'.$block->folder.$tooltip->image; ?>"></div>
				</div>
				<div class="block-feature__tooltip__content">
					<div class="block-feature__tooltip__content-detail">
						<div class="block-feature__tooltip__content-detail__title"><?php echo $tooltip->title; ?></div>
						<div class="block-feature__tooltip__content-detail__description"><?php echo $tooltip->description; ?></div>
					</div>
				</div>
			</div>
		<?php }} ?>
	</div>

	<svg class="svg-feature svg-feature-<?php echo $blockFeature; ?>" viewBox="0 0 1440 1000"></svg>

	<div class="block-title">

		<?php if ( $block->slug == "cushioning" ) { ?>
		<img data-src="/assets/img/<?php echo $block->folder.$block->image; ?>" class="mobile-only">
		<?php } ?>

		<h2 class="theme"><?php echo $blockTitle; ?></h2>
		<h2><?php echo $blockSubtitle; ?></h2>
		<div class="text theme">
			<?php echo $blockDescription; ?>
		</div>

		<?php if ( $block->slug != "cushioning" ) { ?>
		<img data-src="/assets/img/<?php echo $block->folder.$block->image; ?>" class="mobile-only">
		<?php } ?>

		<?php if ( isset($block->tooltips) ) { ?>

			</div><!-- close block-title -->
			<div class="block-slider">

			<div class="slider-tooltips">
			<?php foreach ( $blockTooltips as $tooltip ) { ?>

				<div class="block-slider__slide">
					<div class="block-slider__slide__image">
						<div class="block-slider__slide__bg" data-background="<?php echo '/assets/img/'.$block->folder.$tooltip->image; ?>"></div>
					</div>
					<div class="block-slider__slide__content">
						<div class="block-slider__slide__content-detail">
							<div class="block-slider__slide__content-detail__title"><?php echo $tooltip->title; ?></div>
							<div class="block-slider__slide__content-detail__description"><?php echo $tooltip->description; ?></div>
						</div>
					</div>
				</div>

			<?php } ?>
			</div>
		<?php } ?>

		<div class="button-container">
			<div class="button-top">
				<a href="<?php echo $block->targetMen; ?>" target="_blank">
					<span class="label"><?php echo $block->buttonMen; ?></span>
				</a>
				<a href="<?php echo $block->targetWomen; ?>" target="_blank">
					<span class="label"><?php echo $block->buttonWomen; ?></span>
				</a>
			</div>

			<div class="button-trigger">
				<span class="label"><?php echo $block->button; ?></span>
			</div>

			<span class="background"></span>
		</div>
	</div>
	
</div>
