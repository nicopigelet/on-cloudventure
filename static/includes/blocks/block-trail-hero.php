<div class="block block-trail-hero">
	
	<div class="block-trail-hero__content">

		<div class="block-trail-hero__background" data-background="/assets/img/<?php echo $block->image; ?>"></div>

		<div class="block-trail-hero__title">
			<h3><?php echo $block->location; ?></h3>
			<span class="block-trail-hero__title__separator"></span>
			<h1><?php echo $block->title; ?></h1>
			<div class="text">
				<?php echo $block->description; ?>
			</div>
		</div>
	</div>
	
</div>
