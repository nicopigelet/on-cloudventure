<div class="block block-trail-take">
	
	<div class="block-content">
		<div class="block-trail-take__title">
			<h2 class="extra"><?php echo $trails->ventureTitle; ?></h2>
		</div>
	</div>

	<div class="block-trail-take__trails">
		<?php foreach( $trails->ventures as $venture ) { ?>

			<a href="/trail/<?php echo $venture->slug; ?>" class="block-trail-take__trail" data-background="/assets/img/<?php echo $venture->thumb->image; ?>">
				<span class="block-trail-take__trail__background"></span>
				<span class="block-trail-take__trail__text">
					<span class="h3"><?php echo $venture->thumb->title; ?></span>
					<span class="text"><?php echo $venture->thumb->location; ?></span>
					<span class="block-trail-take__trail__separator"></span>
					<span class="h3 block-trail-take__trail__h3--over"><?php echo $trails->overTitle; ?></span>
					<span class="text block-trail-take__trail__text--over"><?php echo $venture->thumb->description; ?></span>
				</span>
			</a>
		<?php } ?>
	</div>
	
</div>

<div class="block block-trail-take">
	
	<div class="block-content">
		<div class="block-trail-take__title">
			<h2 class="extra"><?php echo $texts->takeTrail; ?></h2>
		</div>
	</div>

	<div class="block-trail-take__trails">
		<?php foreach( $trails->trails as $trail ) { ?>

			<a href="/trail/<?php echo $trail->slug; ?>" class="block-trail-take__trail" data-background="/assets/img/<?php echo $trail->thumb->image; ?>">
				<span class="block-trail-take__trail__background"></span>
				<span class="block-trail-take__trail__text">
					<span class="h3"><?php echo $trail->thumb->title; ?></span>
					<span class="text"><?php echo $trail->thumb->location; ?></span>
					<span class="block-trail-take__trail__separator"></span>
					<span class="h3 block-trail-take__trail__h3--over"><?php echo $trails->overTitle; ?></span>
					<span class="text block-trail-take__trail__text--over"><?php echo $trail->thumb->description; ?></span>
				</span>
			</a>
		<?php } ?>
	</div>
	
</div>
