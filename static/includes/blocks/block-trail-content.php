<div class="block block-trail-content">
	
	<?php foreach( $block->blocks as $content ) {

		switch ($content->type) {

			case "author" :
				echo getAuthor( $content );
			break;

			case "quote" : 
				echo getQuote( $content );
			break;

			case "promo" : 
				echo getPromo( $content );
			break;

			case "paragraph" :
			case "question" : 
			case "answer" : 
			default :
				echo getParagraph( $content, $content->type );
			break;
		}

	} ?>
	
</div>

<?php

function getParagraph ( $content, $type ) {
	$extraClass = $type === 'paragraph' ? '' : ' block-trail-content__'.$type;
	return '<p class="block-trail-content__paragraph'.$extraClass.'">'. $content->content .'</p>';
}

function getAuthor ( $content ) {
	return '<div class="block-trail-content__author">
		<div class="block-trail-content__author__image">
			<img data-src="/assets/img/'. $content->image .'" alt="">
		</div>
		<div class="block-trail-content__author__details">
			<div class="block-trail-content__author__details__name">'. $content->author .'</div>
			<div class="block-trail-content__author__details__bio">'. $content->authorBio .'</div>
			<div class="block-trail-content__author__details__separator"></div>
		</div>
	</div>';
}

function getQuote ( $content ) {
	return '<div class="block-trail-content__quote">'. $content->content .'</div>';
}

function getPromo ( $content ) {
	return '<div class="block-trail-content__promo">
		<a href="'.$content->target.'" target="_blank" class="block-trail-content__promo__image">
			<img data-src="/assets/img/'. $content->image .'" alt="">
		</a>
		<div class="block-trail-content__promo__details">
			<div class="block-trail-content__promo__details__title">'.$content->title.'</div>
			<div class="block-trail-content__promo__details__subtitle">'.$content->subtitle.'</div>
			<div class="block-trail-content__promo__details__separator"></div>
			<a href="'.$content->target.'" target="_blank" class="block-trail-content__promo__details__button">'.$content->button.'</a>
		</div>
	</div>';
}

?>