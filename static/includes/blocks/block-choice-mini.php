<div class="background-choice"></div>
<div class="block block-choice-mini">
	
	<?php foreach ( $choice->shoes as $shoe ) { ?>
	<a class="block-choice-mini__shoe" href="#<?php echo $shoe->id; ?>" data-model="<?php echo $shoe->id; ?>">
		<img data-src="/assets/img/<?php echo $shoe->folder.$shoe->image; ?>" alt="<?php echo $shoe->title; ?>">
		<span class="label"><?php echo $shoe->title; ?></span>
	</a>
	<?php } ?>

	<ul class="block-choice-mini__features">
		<?php foreach ( $features as $feature ) { ?>
		<li class="block-choice-mini__feature theme theme-<?php echo $feature['shoe'][0]; ?>">
			<a href="#<?php echo $feature['slug']; ?>" data-feature="<?php echo $feature['slug']; ?>" class="block-choice-mini__shoe__link-feature">
				<?php echo $feature['name']; ?>
			</a>
		</li>
		<?php } ?>
	</ul>
	
</div>
