<?php
	array_push( $_SESSION['imgToLazyLoad'], '/assets/img/backgrounds/watch-guy.png' );
?>
<div class="block block-watch">

	<div class="block-watch__background" data-background="/assets/img/<?php echo $block->background; ?>"></div>
	<div class="block-watch__guy"></div>

	<div class="block-content">
	
		<div class="block-watch__title">
			<div class="tagline"><?php echo $block->tagline; ?></div>
			<h2 class="extra"><?php echo $block->title; ?></h2>
		</div>

		<a href="#" class="button" data-popin="watch">
			<span class="label"><?php echo $block->button; ?></span>
			<span class="background"></span>
		</a>
	</div>
	
</div>
