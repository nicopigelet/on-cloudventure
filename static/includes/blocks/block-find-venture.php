<?php
	array_push( $_SESSION['imgToLazyLoad'], '/assets/img/backgrounds/shoe_promo.png' );
?>
<div class="block block-find-venture theme-<?php echo implode(' theme-', $block->shoe); ?>">
	
	<div class="block-content">
		<div class="block-find-venture__background"></div>

		<h2 class="block-find-venture__title extra"><?php echo $block->title; ?></h2>

		<div class="block-find-venture__buttons">
			<a class="button" href="<?php echo $block->targetMen; ?>" target="_blank">
				<span class="label"><?php echo $block->buttonMen; ?></span>
				<span class="background"></span>
			</a>
			<a class="button" href="<?php echo $block->targetWomen; ?>" target="_blank">
				<span class="label"><?php echo $block->buttonWomen; ?></span>
				<span class="background"></span>
			</a>
		</div>
	</div>
	
</div>
