<?php
	
	$loading = $block->loading;
	$choice = $block->choice;
?>
<div class="block block-hero" style="background-image: url(/assets/img/<?php echo $loading->background; ?>);" data-choice="/assets/img/<?php echo $choice->background; ?>">

	<div class="block-hero__background"></div>
	<svg class="svg-load" viewBox="0 0 1440 1000"></svg>

	<div class="block-content">

		<div class="loading__content">
		
			<div class="block-title block-title__loading-1">
				<h2><?php echo $loading->text; ?></h2>
			</div>

			<div class="block-title block-title__loading-2">
				<div class="tagline"><?php echo $loading->tagline; ?></div>
				<h2 class="blue"><?php echo $loading->title; ?></h2>
				<h2><?php echo $loading->subtitle; ?></h2>
			</div>
			
			<div class="loading__loader">
				<div class="tagline">
					<?php echo $loading->loading; ?>
					<span class="loading-value">0%</span>
				</div>
			</div>
		</div>

		<div class="choice__content">

			<div class="block-title">
				<h2 class="extra"><?php echo $choice->title; ?></h2>
				<h2 class="extra blue"><?php echo $choice->subtitle; ?></h2>
			</div>

			<?php foreach ( $choice->shoes as $shoeIndex => $shoe ) { ?>

				<div data-model="<?php echo $shoe->id; ?>" data-index="<?php echo $shoeIndex; ?>" class="choice__shoe theme-<?php echo $shoe->id; ?>">
					<span class="choice__shoe-content__title">
						<span class="h2 extra"><?php echo $shoe->title; ?></span>
						<span class="h2 extra theme"><?php echo (isset($shoe->extraSubtitle) ? $shoe->extraSubtitle : $shoe->subtitle); ?></span>
					</span>
					<span class="choice__shoe-content">
						<img class="cover" data-src="/assets/img/<?php echo $shoe->folder.$shoe->image; ?>" alt="<?php echo $shoe->title; ?>">
						<span class="choice__shoe-content__detail">
							<span class="h3"><?php echo $shoe->title; ?></span>
							<span class="text"><?php echo $shoe->subtitle; ?></span>
						</span>
					</span>
					<span class="choice__shoe-content__models">
						<?php foreach ( $shoe->models as $model ) { ?>
							<img data-src="/assets/img/<?php echo $shoe->folder.$model; ?>">
						<?php } ?>
					</span>

					<a href="#<?php echo $shoe->id; ?>" class="button">
						<span class="label"><?php echo $choice->button; ?></span>
						<span class="background"></span>
					</a>
				</div>

			<?php } ?>

		</div>
	</div>
</div>
