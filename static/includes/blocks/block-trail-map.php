<?php
	$intro = $block->intro;
	$map = $block->map;
	$outro = $block->outro;
?>
<div class="block-trail-map__intro">
	
	<div class="block-trail-map__intro__content">
		<div class="block-trail-map__intro__stat block-stat">
			<div class="block-stat__inside">
				<div class="block-stat__number"><?php echo $intro->stat->number; ?></div>
				<div class="block-stat__label"><?php echo $intro->stat->label; ?></div>
			</div>
		</div>
		<div class="block-trail-map__intro__description text">
			<?php echo $intro->description; ?>
		</div>
	</div>
</div>
<!-- class usable : no-miles / no-scroll -->
<div class="block-trail-map__map<?php echo ($map->displayMiles == "true" ? '' : ' no-miles'); echo ($map->scroll == "true" ? '' : ' no-scroll'); ?>" data-file="<?php echo $map->path; ?>">
	<div class="block-trail-map__map__container">
		<div class="block-trail-map__map__miles">
			<div class="block-trail-map__map__miles__header">
				<img data-src="/assets/img/<?php echo $map->image; ?>" alt="On Cloudventure - Trail Shoe">
			</div>
			<div class="block-trail-map__map__miles__scroller">
				<div class="block-trail-map__map__miles__scroller__inside">
				<?php foreach( $map->miles as $mile ) { ?>

					<div class="mile">
						<div class="mile-inside">
							<div class="mile-title"><?php echo $mile->title; ?></div>
							<div class="mile-details">
								<div class="mile-details__elevation">
									<span class="label"><?php echo $map->elevationLabel; ?></span>
									<span class="value"><?php echo $mile->elevationValue; ?></span>
								</div>
								<div class="mile-details__grade">
									<span class="label"><?php echo $map->gradeLabel; ?></span>
									<span class="value"><?php echo $mile->gradeValue; ?></span>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>
				</div>
			</div>
			<div class="block-trail-map__map__miles__promo">
				<div class="block-trail-map__map__miles__promo__inside">
					<span><?php echo $map->poweredBy; ?></span>
					<a href="http://strava.com" target="_blank">
						<img data-src="/assets/img/skin/icon-strava.png" alt="Strava" />
					</a>
				</div>
			</div>
		</div>
		<div class="block-trail-map__map__path"></div>
	</div>
</div>

<div class="block-trail-map__outro">
	<div class="block-trail-map__outro__content">
		<div class="block-trail-map__outro__title">
			<?php echo $outro->title; ?>
		</div>
		<div class="block-trail-map__outro__stats">
		<?php foreach( $outro->stats as $stat ) { ?>
			<div class="block-trail-map__outro__stat block-stat">
				<div class="block-stat__inside">
					<div class="block-stat__number"><?php echo $stat->number; ?></div>
					<div class="block-stat__label"><?php echo $stat->label; ?></div>
				</div>
			</div>
		<?php } ?>
		</div>
		<!--
		<a href="<?php echo $outro->link->target; ?>" class="button button-red">
			<span class="label"><?php echo $outro->link->label; ?></span>
			<span class="background"></span>
		</a>
		-->
	</div>
</div>
