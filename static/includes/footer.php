<div class="footer">
	<span class="tagline">&copy;ON <?php echo date("Y"); ?></span>
	<ul class="lang">
		<li>
			<a href="#en" data-lang="en">EN</a>
		</li>
		<li>
			<a href="#de" data-lang="de">DE</a>
		</li>
		<li>
			<a href="#es" data-lang="es">ES</a>
		</li>
		<li>
			<a href="#fr" data-lang="fr">FR</a>
		</li>
		<li>
			<a href="#it" data-lang="it">IT</a>
		</li>
		<li>
			<a href="#jp" data-lang="jp">JP</a>
		</li>
	</ul>
	<a class="footer__link-right tagline" href="http://on-running.com" target="_blank">
		on-running.com
	</a>
</div>

<div class="overlay overlay-watch">
	<iframe class="overlay-watch__iframe" width="100%" height="100%" data-src="https://www.youtube.com/embed/<?php echo $texts->youtubeID; ?>?autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
	<a href="#" class="overlay-watch__close" data-popin="watch"></a>
</div>