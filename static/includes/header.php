<?php $menu = $texts->header->menu; ?>
<div class="header">

	<div class="menu-cloudventure">

		<a href="/" class="logo">
			<img src="/assets/img/skin/logo-on.png" alt="On Running">
		</a>

		<a href="#" class="btn-open-menu mobile-only"></a>
		<a href="#" class="btn-close-menu mobile-only"></a>
		<a href="#" class="btn-open-master mobile-only" data-action="master"><?php echo $menu->parent; ?></a>
		
		<ul class="menu">
			<li>
				<a data-popin="watch" href="#"><?php echo $menu->watch; ?></a>
			</li>
			<li<?php echo (isset($isTrailPage) && $isTrailPage === true ? ' class="selected"' : ''); ?>>
				<a href="/trail/the-ridge-with-olivier-bernhard" class="btn-take-trail"><?php echo $menu->trail; ?></a>
			</li>
			<li>
				<a href="https://www.on-running.com/t/group/cloudventure"><?php echo $menu->shop; ?></a>
			</li>
			<li class="desktop-only">
				<a href="#" data-action="master" class="btn__header-master"><?php echo $menu->parent; ?></a>
			</li>
		</ul>
	</div>
</div>
<div class="header-master">
	<?php include "header-master.php"; ?>
</div>
