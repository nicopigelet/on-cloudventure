
		<?php 
			$imgToLoad = array_values(array_unique($_SESSION['imgToLoad']));
			$imgToLazyLoad = array_values(array_unique($_SESSION['imgToLazyLoad']));
		?>
		<script type="text/javascript">
			window.DEBUG = false;
			window.imgToLoad = <?php echo json_encode($imgToLoad); ?>;
			window.imgToLazyLoad = <?php echo json_encode($imgToLazyLoad); ?>;
			window.hasLoaded = '<?php echo isset($_SESSION['hasLoaded']) ? $_SESSION['hasLoaded'] : ''; ?>';
		</script>
		
		<script type="text/javascript" src="/assets/js/lib/lib.js"></script>
		<script type="text/javascript" src="/assets/js/main.js?t=<?php echo time(); ?>"></script>

		<?php
		if ( $hasMap == true ) {
			echo '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0A_prWP5Zv1JiUthToBk_v56Y0txMYE4&callback=TrailMap.initMap"></script>';
		}
		?>

		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-35441071-1', 'auto');
		ga('send', 'pageview');

		</script>
	</body>
</html>