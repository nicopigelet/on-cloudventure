<?php $social = $texts->header->social; ?>
<!DOCTYPE html>
<html>
	<head>

		<link rel="shortcut icon" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/favicon.ico?t=<?php echo time(); ?>" type="image/x-icon">
		<link rel="icon" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/favicon.ico?t=<?php echo time(); ?>" type="image/x-icon">

		<meta charset="utf-8" />
		<title><?php echo $social->title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<!-- Facebook -->
		<meta property="og:title" content="<?php echo $social->title; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:image" content="http://cloudventure.on-running.com/social_post.jpg"/>
		<meta property="og:url" content="http://cloudventure.on-running.com/" />
		<meta property="og:description" content="<?php echo $social->description; ?>" />
		<meta property="og:image:width" content="1410"/>
		<meta property="og:image:height" content="738"/>

		<!-- Twitter -->
		<meta name="twitter:title" content="<?php echo $social->title; ?>" />
		<meta name="twitter:card" content="photo" />
		<meta name="twitter:image" content="http://cloudventure.on-running.com/social_post.jpg" />
		<meta name="twitter:url" content="http://cloudventure.on-running.com" />
		<meta name="twitter:description" content="<?php echo $social->description; ?>" />

		<link rel="stylesheet" type="text/css" href="/assets/css/main.css">

		<script type="text/javascript" src="/assets/js/lib/jquery-1.12.4.min.js"></script>

	</head>
	<body class="<?php echo $bodyClass; ?>">
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T82FFR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-T82FFR');</script>
		<!-- End Google Tag Manager -->