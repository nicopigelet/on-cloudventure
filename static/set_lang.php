<?php
	
	$langs = array( 'de', 'en', 'es', 'fr', 'it', 'jp' );
	$lang = 'en';
	
	if ( isset($_GET['lang']) && !empty($_GET['lang']) ) {
		$lang = $_GET['lang'];

		if ( !in_array($lang, $langs) ) {
			$lang = 'en';
		}
		setcookie('venture-language', $lang);
	}
?>