<?php
	session_start();

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	date_default_timezone_set('Europe/Zurich');

	$_SESSION['imgToLoad'] = array('/assets/img/skin/logo-on.png');
	$_SESSION['imgToLazyLoad'] = array();

	include 'get_geoip.php';
	$language = getLanguageByGeoIP();
	$trailLang = ($language == "de") ? $language : "en";
	
	$json_texts_url = "data/".$language."/texts.json";
	$json_trails_url = "data/".$trailLang."/trails.json";
	
	$json_texts = file_get_contents($json_texts_url);
	$texts = json_decode($json_texts);
	
	$json_trails = file_get_contents($json_trails_url);
	$trails = json_decode($json_trails);

	$bodyClass = "template__trail lang__".$language;
	$isTrailPage = true;

	include 'includes/layout/top.php';
	include 'includes/header.php'; ?>

		<div class="wrapper">

			<?php 

				$trailParam = isset($_GET['slug']) ? $_GET['slug'] : '';
				$blocks = $trails->trails[ count($trails->trails)-1 ];
				// $blocks = $trails->trails[ count($trails->trails)-2 ];
				// $blocks = $trails->ventures[ count($trails->ventures)-4 ];

				foreach( $trails->ventures as $trail ) {
					if ( strcmp( $trail->slug, $trailParam ) === 0 ) {
						$blocks = $trail;
					}
				}
				foreach( $trails->trails as $trail ) {
					if ( strcmp( $trail->slug, $trailParam ) === 0 ) {
						$blocks = $trail;
					}
				}

				if ( isset($blocks->page) ) {
					foreach ( $blocks->page as $block ) {
						include 'includes/blocks/block-'.$block->block.'.php';
					}
				}
			?>

			<?php include('includes/footer.php'); ?>
		</div>

<?php 
	$hasMap = true;
	include 'includes/layout/bottom.php'; 
?>