var EventManager = window.EventManager || $({});

var App = window.App || {

	init : function() {

		if ( Utils.isMSIE() ) {
			$('html').addClass('ie');
		}

		Menu.init();
		Lang.init();

		if ( $('body').hasClass('template__home') ) {
			Home.init();
		} else {
			Trail.init();
		}
	}
};

$(window).ready(function(){

	App.init();
});


var Utils = window.Utils || {
	
	debounce : function (func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	},

	map : function (value, start1, stop1, start2, stop2) {
      return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
    },

	is_touch_device : function () {
		return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
	},

	isMSIE : function() {
		
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		}

		var trident = ua.indexOf('Trident/');
		if (trident > 0) {
			var rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}

		var edge = ua.indexOf('Edge/');
		if (edge > 0) {
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		}

		return false;
	},

	isFirefox : function() {
		return /firefox/i.test(navigator.userAgent);
	}
};


var ChoiceHero = window.ChoiceHero || {

	timeoutAnimation : null,
	firstChoice : true,

	blockChoiceContent : null,
	blockHeroChoiceShoe : null,

	currentModel : "",
	currentClass : "",

	init : function() {

		this.initEvents();
	},

	initEvents : function () {

		this.blockChoiceContent = $('.block-hero');
		this.blockHeroChoiceShoe = $('.block-hero .choice__shoe');
		this.blockHeroChoiceShoe
			.on('mouseenter', $.proxy(this.onEnterShoe, this))
			.on('mouseleave', $.proxy(this.onLeaveShoe, this));
	},

	initShoe : function (){
		
		$('.block-hero').addClass('choice');
		this.onShoeChosen( null );
		$('.block-hero .choice__shoe, .block-choice-mini__shoe').on('click', $.proxy(this.onShoeChosen, this) );
	},

	onEnterShoe : function (e) {
		this.blockChoiceContent.addClass('over');
	},

	onLeaveShoe : function (e) {
		this.blockChoiceContent.removeClass('over');
	},

	onShoeChosen : function (e) {

		if ( e ) e.preventDefault();

		var 
			urlModel = (window.location.hash.split('/')[0]).replace('#', ''),
			defaultBlock;

		if ( e ) {
			defaultBlock = $(e.currentTarget);
		} else {

			if ( urlModel !== "" ) {
				defaultBlock = $('.block-hero .choice__shoe[data-model="'+urlModel+'"]');
			} else {
				defaultBlock = $('.block-hero .choice__shoe:eq(0)');
			}
		}

		var model = defaultBlock.length ? defaultBlock.data('model') : $('.block-hero .choice__shoe:eq(0)').data('model');
		
		var themeClass = 'theme-' + model;
		if ( model == this.currentModel ) {
			this.scrollToContent();
			return;
		}

		$('.block-feature').removeClass('visible');
		if ( this.currentClass !== "" ) $('.'+this.currentClass).removeClass('visible');
		$('.'+themeClass).addClass('visible');

		$('.template__loading').removeClass('template__loading');

		$('body')
			.data('model', model)
			.addClass(themeClass)
			.removeClass(this.currentClass);

		$('.block-choice-mini [data-model="'+this.currentModel+'"]').removeClass('selected');
		$('.block-choice-mini [data-model="'+model+'"]').addClass('selected');

		this.currentClass = themeClass;
		this.currentModel = model;
		
		if ( this.firstChoice ) {

			EventManager.trigger('VENTURE_SHOE_CHOSEN');
			this.firstChoice = false;

			if ( urlModel !== "" && urlModel !== "video" ) {
				this.scrollToContent( (window.location.hash.split('/')[1]) );
			}

		} else {

			EventManager.trigger('VENTURE_UPDATE_SHOE', model);
			this.scrollToContent();
		}
	},

	scrollToContent : function( feature ) {

		var blockFeaturePos = feature ? $('.block-feature__'+feature+'.visible').position().top : null;
		var scrollToPosition = blockFeaturePos || $('.block-hero').innerHeight();

		clearTimeout( this.timeoutAnimation );
		this.timeoutAnimation = setTimeout( function(){
			$('html, body').stop().animate({
				'scrollTop' : scrollToPosition
			}, (blockFeaturePos ? 0 : 650) );
		}, 100 );
	}
};

var HomeWaypoints = window.HomeWaypoints || {

	els : [],
	img : [],
	imgTooltips : [],
	top : [],
	imgHeight : [],
	maxImg : [],

	elWatch : null,
	elWatchGuy : null,
	elWatchBackground : null,
	elWatchTop : null,
	elWatchHeight : null,

	elFind : null,
	elFindBackground : null,
	elFindTop : null,
	elFindHeight : null,

	features : null,
	currentIndex : 0,

	activeGrip : false,
	activeUpper : false,
	activeCushion : false,
	activeWatch : null,
	activeFind : null,

	windowH : null,
	windowW : null,

	shoe : "",

	init : function() {

		var self = this;
		self.setShoe( $('body').data('model') );

		self.img = [];
		self.imgTooltips = [];
		self.top = [];
		self.imgHeight = [];
		self.maxImg = [];

		self.els = [
			$('.block-feature__grip.visible'),
			$('.block-feature__upper.visible'),
			$('.block-feature__cushioning.visible')
		];
		$( self.els ).each( function(i, el) {
			var 
				$el = $(el),
				$img = $el.find('.block-image img'),
				$imgTooltips = $el.find('.block-feature__tooltip');

			self.img.push( $img );
			self.imgTooltips.push( $imgTooltips );
			self.top.push( $el.position().top );
			
			var ratio = ( i == 1 ) ? 1 : 1.2;
			self.imgHeight.push( ($img.height() * ratio) );
			self.maxImg.push( $el.data('max') );
		} );
		
		self.elWatch = $('.block-watch');
		self.elWatchTop = self.elWatch.position().top;
		self.elWatchHeight = self.elWatch.height();
		self.elWatchGuy = $('.block-watch__guy');
		self.elWatchBackground = $('.block-watch__background');
		
		self.elFind = $('.block-find-venture.visible');
		self.elFindTop = self.elFind.position().top;
		self.elFindHeight = self.elFind.height();
		self.elFindBackground = self.elFind.find('.block-find-venture__background');

		self.features = $('.block-choice-mini__feature.visible');
		self.features.removeClass('selected');
		self.currentIndex = 0;
		self.features.eq(self.currentIndex).addClass('selected');

		self.windowH = window.innerHeight;
		self.windowW = window.innerWidth;

		Waypoint.destroyAll();

		var waypointHero = new Waypoint({
			element: $('.block-hero'),
			handler: function(direction) {
				EventManager.trigger( ( direction == "down" ) ? 'VENTURE_HERO_OUT' : 'VENTURE_HERO_IN');
			},
			offset: '-65%'
		});

		var inviewGrip = new Waypoint.Inview({
			element: self.els[0],
			enter: function ( direction ) {
				self.activeGrip = true;
				self.updateFeatureMenu(0);
			},
			exited: function() {
				self.activeGrip = false;
			}
		});

		var waypointGripIn = new Waypoint({
			element: self.els[0],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 0, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointGripOut = new Waypoint({
			element: self.els[0],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 0, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewUpper = new Waypoint.Inview({
			element: self.els[1],
			enter: function() {
				self.activeUpper = true;
				self.updateFeatureMenu(1);
			},
			exited: function() {
				self.activeUpper = false;
			}
		});

		var waypointUpperIn = new Waypoint({
			element: self.els[1],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 1, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointUpperOut = new Waypoint({
			element: self.els[1],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 1, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewCushion = new Waypoint.Inview({
			element: self.els[2],
			enter: function() {
				self.activeCushion = true;
				self.updateFeatureMenu(2);
			},
			exited: function() {
				self.activeCushion = false;
			}
		});

		var waypointCushionIn = new Waypoint({
			element: self.els[2],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 2, ( ( direction == "down" ) ? 1 : 0 )] );
			},
			offset: '50%'
		});

		var waypointCushionOut = new Waypoint({
			element: self.els[2],
			handler: function(direction) {
				EventManager.trigger( 'VENTURE_FEATURE_UPDATE', [self.shoe, 2, ( ( direction == "down" ) ? 2 : 1 )] );
			},
			offset: '-75%'
		});

		var inviewWatch = new Waypoint.Inview({
			element: self.elWatch,
			enter: function() {
				self.activeWatch = true;
			},
			exited: function() {
				self.activeWatch = false;
			}
		});

		var inviewFind = new Waypoint.Inview({
			element: self.elFind,
			enter: function() {
				self.activeFind = true;
			},
			exited: function() {
				self.activeFind = false;
			}
		});
	},

	updateFeatureMenu : function ( index ) {

		this.features.eq(this.currentIndex).removeClass('selected');
		this.features.eq(index).addClass('selected');
		this.currentIndex = index;

		var model = $('body').data('model');
		if ( model !== '' ) {
			var selectedfeature = this.els[index].data('feature') || null;
			var hash = model + (selectedfeature ? ('/'+ selectedfeature) : '' );
			window.location.hash = hash;
		}
	},

	setShoe : function( shoe ) {

		this.shoe = shoe;
	},

	update : function ( clientHeight, scrollTop, scrollHeight ) {

		if ( this.activeWatch ){
			this.updateWatch( scrollTop, clientHeight );
		}

		if ( this.activeFind ){
			this.updateFind( scrollTop, clientHeight );
		}

		if ( !this.activeGrip && !this.activeUpper && !this.activeCushion ) return;

		if ( this.activeGrip ){
			this.update360( 0, scrollTop, clientHeight );
		}

		if ( this.activeUpper ){
			this.update360( 1, scrollTop, clientHeight );
		}

		if ( this.activeCushion ){
			this.update360( 2, scrollTop, clientHeight );
		}
	},

	updateWatch : function ( scrollTop, clientHeight ) {

		var translateValGuy = Utils.map( scrollTop, this.elWatchTop - clientHeight, this.elWatchTop+this.elWatchHeight, 280, 20 );
		this.elWatchGuy.css('transform', 'translateY('+translateValGuy+'px)');

		if ( this.windowW < 1024 ) return;
		this.elWatchBackground.css('transform', 'translateY('+(-translateValGuy+20)+'px)');
	},

	updateFind : function ( scrollTop, clientHeight ) {

		var translateVal = Utils.map( scrollTop, this.elFindTop - clientHeight, this.elFindTop+this.elFindHeight, 5, -30 );
		this.elFindBackground.css('transform', 'translateY('+translateVal+'%)');
	},

	update360 : function ( index, scrollTop, clientHeight ) {

		var 
			$img = this.img[index],
			$imgTooltips = this.imgTooltips[index],
			top = this.top[index], 
			imgHeight = this.imgHeight[index], 
			maxImg = this.maxImg[index],
			min = top - clientHeight + imgHeight,
			max = min + 280,
			percent = 0,
			shoe = this.shoe,
			indexSequence = index + 1;

		if ( scrollTop > min && scrollTop < max ) {
			
			percent = Math.ceil( maxImg * (scrollTop - min) / (max - min) );
			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-' + percent + '.png' );
			$imgTooltips.removeClass('visible');

		} else if ( scrollTop <= min ) {

			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-1.png' );

		} else if ( scrollTop >= max ) {
			
			$img.attr( 'src', '/assets/img/'+shoe+'/'+shoe+'-sequence-'+indexSequence+'-' + maxImg + '.png' );
			$imgTooltips.addClass('visible');
		}
	}
};


var Lang = window.Lang || {
	
	init : function() {

		$('.lang a').on('click', function(e){
			e.preventDefault();
			$.ajax( {
				url : '/set_lang.php',
				data : {
					'lang': $(this).data('lang') 
				},
				success : function() {
					window.location.reload(false);
				}
			});
		});
	}
};
var Loader = window.Loader || {

	loading : null,

	maxImg : 0,
	imgLoaded : 0,
	$percentage : null,
	$title1 : null,
	$title2 : null,

	maxImgLazy : 0,
	imgLazyLoaded : 0,
	
	init : function() {

		this.$title1 = $('.block-hero .loading__content .block-title__loading-1');
		this.$title2 = $('.block-hero .loading__content .block-title__loading-2');
		this.$percentage = $('.loading-value');

		this.loading = {
			load : 0,
			update : 0
		};

		// if ( (window.imgToLoad && window.imgToLoad.length > 0) /*&& window.hasLoaded === ''*/ ) {

		var imgToLoad = $(':not(iframe)[data-src], [data-background], [data-choice]');

		if ( imgToLoad.length > 0 && window.hasLoaded === '' ) {
		
			this.maxImg = imgToLoad.length;
			var self = this;

			$(imgToLoad).each( function( i, src ) {

				var $src = $(src);
				var img = new Image();
				img.alt = $src.data('src') ? 'src' : ( $src.data('background') ? 'background' : 'choice' );
				img.onload = $.proxy(self.onImgLoaded, self);
				img.onerror = $.proxy(self.onImgLoaded, self);
				img.src = $src.data('src') || $src.data('background') || $src.data('choice');
			} );

			this.rafUpdate = true;
			requestAnimationFrame( $.proxy(this.updateLoad, this) );

		} else {

			$('img[data-src]').each(function(i, src){
				$(src).attr('src', $(src).data('src'));
			});

			$('div[data-background], a[data-background]').each(function(i, src){
				$(src).css('background-image', 'url('+$(src).data('background')+')');
			});

			EventManager.trigger('VENTURE_LOADING', 100);
			this.onLoadComplete();
		}
	},

	updateLoad : function() {

		if ( this.loading.update >= 100 ) {
			this.onLoadComplete(); 
			return;
		}

		if ( this.loading.update <= this.loading.load ) {
			this.loading.update += ( window.DEBUG ) ? 100 : 0.4;
		}
		this.onLoad();
		
		requestAnimationFrame( $.proxy(this.updateLoad, this) );
	},

	onImgLoaded : function (e) {

		var 
			type = e.currentTarget.alt,
			src = $(e.currentTarget).attr('src');

		if ( type == "src" ) {
			$('[data-'+type+'="'+src+'"]').attr('src', src);
		} else if ( type == "background" ) {
			$('[data-'+type+'="'+src+'"]').css('background-image', 'url('+src+')');
		}

		this.imgLoaded++;
		this.loading.load = this.imgLoaded * 100 / this.maxImg;
	},

	onLoad : function () {

		var percent = (this.loading.update >> 0);
		
		if ( percent >= 2 && percent < 50 ) {
			this.$title1.addClass('visible');
		} else {
			this.$title1.removeClass('visible');
		}
		if ( percent >= 55 ) {
			this.$title2.addClass('visible');
		}

		this.$percentage.text( percent+'%' );
		EventManager.trigger('VENTURE_LOADING', percent);
	},

	onLoadComplete : function () {

		EventManager.trigger('VENTURE_LOADED');
		this.initLazyLoad();
	},

	initLazyLoad : function () {

		var self = this;

		self.maxImgLazy = window.imgToLazyLoad.length;
		$(window.imgToLazyLoad).each( function( i, src ) {

			var img = new Image();
			img.onload = $.proxy(self.onImgLazyLoaded, self);
			img.onerror = $.proxy(self.onImgLazyLoaded, self);
			img.src = src;
		} );
	},

	onImgLazyLoaded : function () {
		this.imgLazyLoaded++;
		if ( this.imgLazyLoaded >= this.maxImgLazy ) {
			console.log('lazy load complete');
			EventManager.trigger('VENTURE_LAZY_LOADED');
		}
	}

};

var Menu = window.Menu || {

	timeoutOverlay : null,
	previousHash : "",
	
	init : function() {

		this.initFixHeaderMaster();
		this.initMasterHeaderButton();
		this.initWatchButton();

		this.initMobileMenu();

		this.initEscKey();
	},

	initMobileMenu : function () {

		$('.btn-open-menu').on('click', function(e){
			e.preventDefault();
			$('.header').addClass('menu-open');
		});

		$('.btn-close-menu').on('click', function(e){
			e.preventDefault();
			$('.header').removeClass('menu-open');
		});
	},

	initFixHeaderMaster : function () {

		$('.header-master a').each( function(i, link) {

			var 
				$link = $(link),
				href = $link.attr('href');

			if ( href.match( /^\// ) ) {
				$link.attr('href', 'http://on-running.com'+href);
			}
		});

		$('.logo').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = "/";
		});

		$('.btn-take-trail').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = $('.btn-take-trail').attr('href');
		});
		
		$('.block-trail-take__trail').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			window.location.href = $(this).attr('href');
		});
	},

	initEscKey : function () {
		var self = this;
		$('body').on('keyup', function(e) {
			if ( e.keyCode == '27' && $('.overlay-watch').hasClass('visible') ) self.toggleVideoOverlay(null);
			if ( e.keyCode == '27' && $('.header-master').hasClass('visible') ) self.toggleHeaderMaster(null);
		});
	},

	initMasterHeaderButton : function () {
		
		$('[data-action="master"]').on('click', $.proxy(this.toggleHeaderMaster, this));
	},

	toggleHeaderMaster : function (e) {

		if ( e ) e.preventDefault();

		$('body').toggleClass('header-master__open');

		if ( $(e.target).hasClass('btn-open-master') ) {
			$('html, body').stop(true, true).animate({
				scrollTop : 0
			}, 200);
		}
	},

	initWatchButton : function () {

		var urlModel = (window.location.hash.split('/')[0]).replace('#', '');
		if ( urlModel == "video" ) this.toggleVideoOverlay();
		
		$('[data-popin="watch"]').on('click', $.proxy(this.toggleVideoOverlay, this));
	},

	toggleVideoOverlay : function (e) {

		if (e) e.preventDefault();

		clearTimeout( this.timeoutOverlay );

		var 
			$overlay = $('.overlay-watch'),
			$frame = $overlay.find('iframe');

		if ( $overlay.hasClass('visible') ) {
			
			$frame.attr('src', '');
			$overlay.removeClass('visible');
			if ( this.previousHash ) window.location.hash = this.previousHash;

		} else {
			$overlay.addClass('visible');
			this.previousHash = window.location.hash;
			window.location.hash = "video";

			this.timeoutOverlay = setTimeout( function(){
				$frame.attr('src', $frame.data('src'));
			}, 200 );
		}
	}
};

var MiniChoice = window.MiniChoice || {

	$dom : null,
	$choice : null,
	$background : null,

	init : function () {

		this.$dom = $('.block-choice-mini, .background-choice');
		this.$choice = $('.block-choice-mini');
		this.$background = $('.background-choice');

		this.initRollover();
		this.initClick();
	},

	initClick : function() {

		this.$choice.find('.block-choice-mini__shoe__link-feature').on('click', function(e){

			e.preventDefault();

			var $feature = $('.block-feature.visible[data-feature='+$(this).data('feature')+']');
			$('html, body').stop(true,true).animate({
				scrollTop : $feature.position().top
			}, 600);

		});
	},

	initRollover : function() {

		var self = this;
		this.$choice.on('mouseenter', function(){
			self.$background.addClass('is-visible');
		});

		this.$choice.on('mouseleave', function(){
			self.$background.removeClass('is-visible');
		});
	},

	update : function (ch, st) {

		if ( st > ch ) {
			this.$dom.addClass('is-fixed');
		} else {
			this.$dom.removeClass('is-fixed');
		}
	}
};

  // RAIN ANIMATION

  /*
    gravity : 0.4,
    wind : 0.015,
    rain_chance : 0.3,
  */  
  var Config = {
    run : false,

    gravity : 0.25,
    wind : 0.03,
    rain_chance : 0.6,
    drop_size : 1,

    drop_colour : 'rgba(255,255,255,0.5)'
  };

  window.requestAnimFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function(callback) {
          window.setTimeout(callback, 1000 / 60);
      };

  //--------------------------------------------

  var Vector = function(x, y) {

    this.x = x || 0;
    this.y = y || 0;
  };

  Vector.prototype.add = function(v) {

    if (v.x != null && v.y != null) {

      this.x += v.x;
      this.y += v.y;

    } else {

      this.x += v;
      this.y += v;
    }

    return this;
  };

  Vector.prototype.copy = function() {

    return new Vector(this.x, this.y);
  };

  //--------------------------------------------

  var Rain = function() {

    this.pos = new Vector(Math.random() * RainAnimation.canvas.width, -50);
    this.prev = this.pos;

    this.vel = new Vector();
  };

  Rain.prototype.update = function() {

    this.prev = this.pos.copy();

    this.vel.y += Config.gravity;
    this.vel.x += Config.wind;

    this.pos.add(this.vel);
  };

  Rain.prototype.draw = function() {

    RainAnimation.ctx.beginPath();
    RainAnimation.ctx.moveTo(this.pos.x, this.pos.y);
    RainAnimation.ctx.lineTo(this.prev.x, this.prev.y);
    RainAnimation.ctx.stroke();
  };

  //--------------------------------------------

  var Drop = function(x, y) {

    var dist = Math.random() * 7;
    var angle = Math.PI + Math.random() * Math.PI;

    this.pos = new Vector(x, y);

    this.vel = new Vector(
      Math.cos(angle) * dist,
      Math.sin(angle) * dist
      );
  };

  Drop.prototype.update = function() {

    this.vel.y += Config.gravity;

    this.vel.x *= 0.95;
    this.vel.y *= 0.95;

    this.pos.add(this.vel);
  };

  Drop.prototype.draw = function() {

    RainAnimation.ctx.beginPath();
    RainAnimation.ctx.arc(this.pos.x, this.pos.y, 1, 0, Math.PI * 2);
    RainAnimation.ctx.fill();
  };

  //--------------------------------------------

  var RainAnimation = {

    canvas : null,
    rain : [],
    drops : [],

    init : function () {

      EventManager.on('VENTURE_FEATURE_UPDATE', $.proxy( this.onUpdate, this ));
    },

    onUpdate : function (e, shoe, feature, position) {

      if ( feature !== 1 ) return;

      RainAnimation.canvas = $('.canvas-rain')[0];

      switch ( position ) {
        case 1:
          RainAnimation.start();
        break;

        case 0:
        case 2:
          RainAnimation.stop();
        break;
        default:
          RainAnimation.stop();
        break;
      }
    },

    update : function() {

      RainAnimation.ctx.clearRect(0, 0, RainAnimation.canvas.width, RainAnimation.canvas.height);

      var i = RainAnimation.rain.length;
      while (i--) {

        var raindrop = RainAnimation.rain[i];

        raindrop.update();

        if (raindrop.pos.y >= RainAnimation.canvas.height) {

          var n = Math.round(4 + Math.random() * 4);

          while (n--)
          RainAnimation.drops.push(new Drop(raindrop.pos.x, RainAnimation.canvas.height));

          RainAnimation.rain.splice(i, 1);
        }

        raindrop.draw();
      }

      var j = RainAnimation.drops.length;
      while (j--) {

        var drop = RainAnimation.drops[j];
        drop.update();
        drop.draw();

        if (drop.pos.y > RainAnimation.canvas.height) RainAnimation.drops.splice(j, 1);
      }

      if (Math.random() < Config.rain_chance) RainAnimation.rain.push(new Rain());

      if ( Config.run ) window.requestAnimFrame( RainAnimation.update );
      else RainAnimation.ctx.clearRect(0, 0, RainAnimation.canvas.width, RainAnimation.canvas.height);
    },

    start : function (  ) {
      
      Config.run = true;
      
      RainAnimation.ctx = RainAnimation.canvas.getContext('2d');
      RainAnimation.canvas.width = 1600;
      RainAnimation.canvas.height = 720;

      RainAnimation.ctx.lineWidth = Config.drop_size;
      RainAnimation.ctx.strokeStyle = Config.drop_colour;
      RainAnimation.ctx.fillStyle = Config.drop_colour;

      RainAnimation.update();
    },

    stop : function () {

      Config.run = false;
    }

  };



var SVGLines = window.SVGLines || {

	container : [],
	path : [],

	paths : [
		[],
		[],
		[],// cloudventure-waterproof
		[],// cloudventure-peak
		[//cloudventure
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,731.708l70.062-55.412l10.863-6.975l132.798-68.318l52.571-27.158l30.895-16.194l45.346-25.412l52.82-30.396l29.649-16.444l20.729-12.208l23.669-10.217l69.514-27.156l63.035-24.417l5.083-1.545l140.271-24.167l60.046-10.466l14.65-2.241l98.813-58.751l129.459-76.09l16.594-9.618l0.051-0.05l67.47-20.131l20.681-9.469l43.054-24.416l39.117-20.93l30.197-13.204l33.885-15.697l42.057-18.437l15.348-13.854l47.838-41.907l7.425-7.125l25.114-43.054",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],
		[//cloudventure-midtop
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,731.708l70.062-55.412l10.863-6.975l132.798-68.318l52.571-27.158l30.895-16.194l45.346-25.412l52.82-30.396l29.649-16.444l20.729-12.208l23.669-10.217l69.514-27.156l63.035-24.417l5.083-1.545l140.271-24.167l60.046-10.466l14.65-2.241l98.813-58.751l129.459-76.09l16.594-9.618l0.051-0.05l67.47-20.131l20.681-9.469l43.054-24.416l39.117-20.93l30.197-13.204l33.885-15.697l42.057-18.437l15.348-13.854l47.838-41.907l7.425-7.125l25.114-43.054",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],
		//cloudventure-waterproof,
		[
			"",
			"",
			""
		],
		[//cloudventure-peak
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,731.708l70.062-55.412l10.863-6.975l132.798-68.318l52.571-27.158l30.895-16.194l45.346-25.412l52.82-30.396l29.649-16.444l20.729-12.208l23.669-10.217l69.514-27.156l63.035-24.417l5.083-1.545l140.271-24.167l60.046-10.466l14.65-2.241l98.813-58.751l129.459-76.09l16.594-9.618l0.051-0.05l67.47-20.131l20.681-9.469l43.054-24.416l39.117-20.93l30.197-13.204l33.885-15.697l42.057-18.437l15.348-13.854l47.838-41.907l7.425-7.125l25.114-43.054",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],

		[//cloudventure
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,0.6 L0.799,0 24.887,28.586 49.174,48.575 109.443,96.5 150.072,118.789 214.438,147.174 241.774,187.453 324.88,218.386 395.194,239.675 465.158,262.664 495.192,273.358 495.242,273.408 543.217,304.042 643.215,382.701 689.141,418.982 727.771,438.622 775.746,461.61 775.795,461.66 817.424,492.994 844.11,514.982 895.134,563.656 950.105,615.629 961.299,627.174 1014.472,639.467 1061.098,647.113 1080.837,650.461 1122.865,663.154 1150.751,674.799 1226.961,683.094 1301.271,691.09 1345.35,696.139 1345.399,696.139 1400.47,722.523 1400.521,722.574 1440,762.203",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],
		[//cloudventure-midtop
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,0.6 L0.799,0 24.887,28.586 49.174,48.575 109.443,96.5 150.072,118.789 214.438,147.174 241.774,187.453 324.88,218.386 395.194,239.675 465.158,262.664 495.192,273.358 495.242,273.408 543.217,304.042 643.215,382.701 689.141,418.982 727.771,438.622 775.746,461.61 775.795,461.66 817.424,492.994 844.11,514.982 895.134,563.656 950.105,615.629 961.299,627.174 1014.472,639.467 1061.098,647.113 1080.837,650.461 1122.865,663.154 1150.751,674.799 1226.961,683.094 1301.271,691.09 1345.35,696.139 1345.399,696.139 1400.47,722.523 1400.521,722.574 1440,762.203",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],
		//cloudventure-waterproof
		[
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,0.6 L0.799,0 24.887,28.586 49.174,48.575 109.443,96.5 150.072,118.789 214.438,147.174 241.774,187.453 324.88,218.386 395.194,239.675 465.158,262.664 495.192,273.358 495.242,273.408 543.217,304.042 643.215,382.701 689.141,418.982 727.771,438.622 775.746,461.61 775.795,461.66 817.424,492.994 844.11,514.982 895.134,563.656 950.105,615.629 961.299,627.174 1014.472,639.467 1061.098,647.113 1080.837,650.461 1122.865,663.154 1150.751,674.799 1226.961,683.094 1301.271,691.09 1345.35,696.139 1345.399,696.139 1400.47,722.523 1400.521,722.574 1440,762.203",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		],
		[//cloudventure-peak
			"M0,0 L45,0 90,0 135,0 180,0 225,0 270,0 315,0 360,0 405,0 450,0 495,0 540,0 585,0 630,0 675,0 720,0 765,0 810,0 855,0 900,0 945,0 990,0 1035,0 1080,0 1125,0 1170,0 1215,0 1260,0 1305,0 1350,0 1395,0 1440,0",
			"M0,0.6 L0.799,0 24.887,28.586 49.174,48.575 109.443,96.5 150.072,118.789 214.438,147.174 241.774,187.453 324.88,218.386 395.194,239.675 465.158,262.664 495.192,273.358 495.242,273.408 543.217,304.042 643.215,382.701 689.141,418.982 727.771,438.622 775.746,461.61 775.795,461.66 817.424,492.994 844.11,514.982 895.134,563.656 950.105,615.629 961.299,627.174 1014.472,639.467 1061.098,647.113 1080.837,650.461 1122.865,663.154 1150.751,674.799 1226.961,683.094 1301.271,691.09 1345.35,696.139 1345.399,696.139 1400.47,722.523 1400.521,722.574 1440,762.203",
			"M0,1000 L45,1000 90,1000 135,1000 180,1000 225,1000 270,1000 315,1000 360,1000 405,1000 450,1000 495,1000 540,1000 585,1000 630,1000 675,1000 720,1000 765,1000 810,1000 855,1000 900,1000 945,1000 990,1000 1035,1000 1080,1000 1125,1000 1170,1000 1215,1000 1260,1000 1305,1000 1350,1000 1395,1000 1440,1000"
		]
	],

	init : function () {

		var self = this;

		this.numShoes = $('.choice__content .choice__shoe').length;

		$('.block-feature').each( function ( i, feature ) {

			self.container[i] = Snap('.svg-feature-'+(i+1));

			self.path[i] = self.container[i].path( self.paths[i][0] )
						.attr({
							fill: "none",
							stroke: "#FFF",
							strokeWidth: 1,
							opacity : 0.5
						});
		} );

		EventManager.on('VENTURE_FEATURE_UPDATE', $.proxy( this.onUpdate, this ));
	},

	onUpdate : function (e, shoe, feature, position) {

		// if ( shoe == 'cloudventure-waterproof' ) return;

		var offset = $('.choice__shoe[data-model="'+shoe+'"]').data('index');
		feature = (feature*this.numShoes)+offset;

		var toPath = this.paths[feature][position];

		if ( toPath === "" || toPath === undefined ) return;

		var duration = feature === 0 ? 300 : 1000;
		var self = this;
		
		this.path[feature]
			.attr({
				opacity : 0.5
			})
			.animate({
				d : toPath
			}, duration, mina.easeinout, function(){
				if ( position == 2 ) {
					this.attr({
						opacity : 0
					});
				}
			});
	}
};

var SVGPath = window.SVGPath || {
	
	paths : [
		'M0,922.686 L9.082,916.771 18.164,910.855 24.409,905.693 30.655,900.532 41.007,882.779 51.359,865.026 59.335,859.801 67.312,854.574 69.663,852.725 72.013,850.874 88.166,845.223 104.318,839.571 109.443,832.82 114.569,826.069 124.396,822.494 134.224,818.918 139.575,833.295 144.926,847.673 147.176,847.673 149.426,847.673 153.051,845.022 156.677,842.372 158.873,838.787 161.07,835.203 162.824,834.537 164.579,833.871 167.354,830.721 170.13,827.569 171.18,826.744 172.229,825.92 175.105,825.545 177.981,825.169 181.465,822.613 184.948,820.059 188.266,820.639 191.583,821.218 192.108,819.943 192.634,818.668 198.41,818.793 204.186,818.918 206.062,817.418 207.937,815.918 210.437,816.117 212.937,816.317 215.55,814.742 218.163,813.168 223.927,813.168 229.69,813.168 232.966,814.742 236.241,816.317 240.092,815.293 243.942,814.268 247.067,816.367 250.193,818.468 252.019,818.218 253.844,817.968 257.245,821.844 260.646,825.72 262.496,828.82 264.346,831.92 266.096,834.045 267.846,836.171 269.497,840.446 271.147,844.722 276.073,848.498 280.999,852.273 282.949,851.349 284.899,850.424 288.35,850.299 291.801,850.174 292.776,849.074 293.751,847.974 295.151,848.299 296.552,848.623 299.202,851.523 301.853,854.424 303.253,858.125 304.653,861.825 303.853,853.249 303.053,844.673 303.428,842.147 303.803,839.622 308.304,834.121 312.805,828.62 312.68,820.543 312.555,812.467 315.931,810.416 319.306,808.366 320.356,807.441 321.406,806.516 327.007,809.066 332.607,811.617 342.734,814.867 352.861,818.118 361.063,823.844 369.265,829.569 375.69,832.346 382.116,835.121 385.792,836.221 389.468,837.321 392.268,844.072 395.068,850.824 397.618,858.775 400.169,866.727 416.047,877.254 431.925,887.78 435.625,896.057 439.326,904.333 443.027,923.836 446.729,943.34 454.229,950.366 461.729,957.393 467.98,964.219 474.232,971.045 481.034,971.971 487.835,972.896 495.136,981.473 502.438,990.048 514.839,991.998 527.241,993.949 543.77,996.975 560.298,1000 565.424,993.898 570.55,987.797 577.801,960.067 585.052,932.338 594.403,923.361 603.755,914.385 613.182,911.185 622.608,907.984 634.71,896.332 646.812,884.68 655.289,873.777 663.766,862.876 671.417,849.898 679.068,836.922 686.27,833.972 693.471,831.021 695.021,832.871 696.571,834.721 703.272,845.647 709.974,856.574 722.025,854.799 734.077,853.024 736.228,852.1 738.378,851.174 744.33,836.797 750.281,822.419 758.632,818.094 766.982,813.768 781.686,824.395 796.389,835.021 802.689,843.723 808.991,852.425 813.893,852.549 818.793,852.674 832.67,842.397 846.547,832.121 854.048,813.543 861.549,794.964 879.902,778.562 898.257,762.159 924.961,736.629 951.666,711.1 968.068,683.495 984.471,655.891 996.072,635.236 1007.675,614.583 1021.701,604.981 1035.729,595.38 1047.831,594.205 1059.934,593.029 1061.709,593.18 1063.485,593.329 1069.736,604.355 1075.986,615.383 1089.715,637.512 1103.442,659.641 1104.793,661.091 1106.143,662.541 1116.119,666.941 1126.096,671.343 1141.773,684.846 1157.451,698.348 1174.778,709.875 1192.105,721.401 1186.931,730.152 1181.756,738.904 1178.53,746.355 1175.305,753.808 1174.454,758.309 1173.604,762.809 1178.529,771.21 1183.455,779.611 1193.357,793.964 1203.259,808.316 1204.285,810.167 1205.311,812.018 1208.636,822.395 1211.961,832.771 1212.786,836.447 1213.611,840.122 1231.89,852.975 1250.168,865.826 1273.396,871.652 1296.625,877.479 1314.179,896.857 1331.732,916.235 1335.683,923.836 1339.633,931.438 1362.862,875.252 1386.092,819.067 1389.092,813.416 1392.092,807.766 1398.418,797.014 1404.744,786.263 1406.045,783.262 1407.345,780.262 1407.42,775.911 1407.494,771.561 1412.245,750.156 1416.996,728.753 1416.821,726.252 1416.646,723.752 1424.322,704.674 1431.999,685.595 1432.248,658.416 1432.498,631.236 1436.249,622.021 1440,612.808',
		'M0,1000 L10.75,998 21.35,992.25 33.6,989.15 39.6,986.35 64.75,985.3 82.5,981.75 86,978.25 106.95,972.75 112,968.3 139.35,951.5 146.2,951.5 148.25,949 149.7,950.449 155,946.199 159.15,944.75 168.75,938.1 174.1,935.55 178.65,935.55 187.65,929.55 192.15,929.55 198.9,924.8 216.5,926.05 224.95,925.8 232.5,927.5 238.25,933.75 240.95,934.35 242.5,932.55 245.05,932.4 256.4,942.35 258.5,943.55 260.25,943.55 262.25,946.8 267.3,949.85 272.25,948.5 276.05,944.699 279.7,947.25 285.5,948.199 289.3,947.449 296.3,946.65 301.9,948.55 320.45,948.55 328.15,949.3 331,950.949 336.15,950.9 344,948.449 352.5,945.199 356.8,942.05 360.55,943.75 370.75,943.65 374.4,943.199 378.7,937.55 380.55,938.15 383.75,940.199 393.9,939.8 397.75,938.449 401.65,940.65 420.05,942.35 421.2,941.199 425.7,941.949 427.35,945.3 429.25,945.65 433.45,950.55 438.25,950.699 440.8,954.65 446.366,961.925 454.6,966.35 456.85,966.05 457,969.25 462.2,973.6 467.45,973.449 470.35,973.15 474.6,977.4 477.1,984.15 483.65,981.3 488.3,980.949 489.95,978.6 494.75,978.6 498.4,975.1 501.3,975.1 503,972.8 506,974.85 509.4,974.949 514.5,977.9 517.55,976.35 521.6,977.699 535.35,979.3 544.25,978.8 548.4,976.75 550.35,976.15 556.2,971.449 559.65,971.05 572.15,971.05 576.6,969.1 578.3,968.55 586.3,960.75 595.65,954.9 600.75,949.85 602,947.699 604.65,950.35 606.45,948.699 608.1,951.75 612.1,947.3 618.45,936.5 625.4,934.15 630.2,932.25 637.15,935.35 641.7,933.071 663.7,935.449 679.7,939 684.1,943.4 693.2,946.449 698.95,946.55 708.75,953.4 714.75,953.4 718.95,951.25 722.95,947.25 733.5,945.199 741.7,944.199 748.55,945.3 756.35,946.05 763.4,945.949 768.75,947.05 783,947.1 790.45,945.6 797.95,945 802.9,949.449 807.5,949.699 812,953.949 820.2,954.35 824.95,956.449 830,956.949 837.2,962.3 844.4,955.6 847.95,954.9 854.5,959.65 857.45,961.199 860.75,961.8 863.051,959.25 870.85,958.4 875.1,959.9 879.4,966.6 882.75,966.5 887,969.9 891.5,972.199 894.9,973.5 902.2,973.75 907.7,970.85 917.051,971.35 919.5,974.75 925.2,977.699 929.95,978.65 939.95,978.75 944.65,975.5 952.85,977.5 955.301,977.5 958.301,974.1 963,969.699 968.4,971.65 974.051,976.199 977.101,976.4 980.851,973.949 990,973.949 991.95,974.9 995.301,977.15 996.9,975.1 999,975.449 1000.5,976.699 1006.1,976.949 1015.95,984.65 1017.551,986.4 1021.15,986.4 1023.65,984.9 1026.25,984.9 1029.301,981.55 1032.25,982.25 1036.95,983.949 1041.7,986.199 1044.7,986.85 1053.3,987.1 1058.15,980.699 1063.8,977.15 1069.95,976.75 1074.6,978.25 1076.6,979.35 1076.9,981.15 1082.05,981.05 1083.15,979.15 1085.3,978.699 1087.75,975.55 1089.8,975.75 1092.65,978.199 1096.7,977.15 1107.2,978.949 1116.95,990.4 1129.2,991.199 1133.7,991.199 1139.7,989.6 1147.85,987.8 1149.2,986.35 1153.6,988.65 1156.5,988.25 1160.95,986.449 1165.8,981.75 1174.05,981.5 1177.75,983.9 1184.5,977.05 1188.35,975.15 1190.5,975.05 1191.65,970.6 1199.35,964.75 1203.95,962.9 1214.25,962.65 1221.5,965.1 1225.55,967.449 1234.1,974.1 1240.8,975.5 1247,977.25 1250.4,978.5 1254.35,979.75 1258.55,981.75 1263.1,983.85 1265.95,986.05 1268.65,985.8 1272.8,986.75 1276.35,990.699 1297.5,990.1 1302.15,988.8 1309.85,979.949 1312.1,976.699 1316.45,971.65 1320.45,967.9 1322.4,968.75 1325.8,967.949 1330.4,965.8 1332.45,964.15 1338.5,965.199 1343.85,970.699 1345.75,968.05 1348.15,966.4 1352.2,969.25 1354.3,972.4 1356.8,972.4 1359.55,976.1 1362.35,974.15 1367.45,973.4 1368.35,972.15 1386.25,970.4 1388.15,969 1395.1,969.1 1403.15,973.85 1404.65,968.75 1409.5,970.75 1416.45,966 1419.15,965.75 1421.4,962.35 1425.45,962.05 1426.551,959.35 1428.301,959.35 1430.25,957.65 1434.7,958.4 1440,955.159',
		'M0,1000 L5.625,1000 11.25,1000 16.875,1000 22.5,1000 28.125,1000 33.75,1000 39.375,1000 45,1000 50.625,1000 56.25,1000 61.875,1000 67.5,1000 73.125,1000 78.75,1000 84.375,1000 90,1000 95.625,1000 101.25,1000 106.875,1000 112.5,1000 118.125,1000 123.75,1000 129.375,1000 135,1000 140.625,1000 146.25,1000 151.875,1000 157.5,1000 163.125,1000 168.75,1000 174.375,1000 180,1000 185.625,1000 191.25,1000 196.875,1000 202.5,1000 208.125,1000 213.75,1000 219.375,1000 225,1000 230.625,1000 236.25,1000 241.875,1000 247.5,1000 253.125,1000 258.75,1000 264.375,1000 270,1000 275.625,1000 281.25,1000 286.875,1000 292.5,1000 298.125,1000 303.75,1000 309.375,1000 315,1000 320.625,1000 326.25,1000 331.875,1000 337.5,1000 343.125,1000 348.75,1000 354.375,1000 360,1000 365.625,1000 371.25,1000 376.875,1000 382.5,1000 388.125,1000 393.75,1000 399.375,1000 405,1000 410.625,1000 416.25,1000 421.875,1000 427.5,1000 433.125,1000 438.75,1000 444.375,1000 450,1000 455.625,1000 461.25,1000 466.875,1000 472.5,1000 478.125,1000 483.75,1000 489.375,1000 495,1000 500.625,1000 506.25,1000 511.875,1000 517.5,1000 523.125,1000 528.75,1000 534.375,1000 540,1000 545.625,1000 551.25,1000 556.875,1000 562.5,1000 568.125,1000 573.75,1000 579.375,1000 585,1000 590.625,1000 596.25,1000 601.875,1000 607.5,1000 613.125,1000 618.75,1000 624.375,1000 630,1000 635.625,1000 641.25,1000 646.875,1000 652.5,1000 658.125,1000 663.75,1000 669.375,1000 675,1000 680.625,1000 686.25,1000 691.875,1000 697.5,1000 703.125,1000 708.75,1000 714.375,1000 720,1000 725.625,1000 731.25,1000 736.875,1000 742.5,1000 748.125,1000 753.75,1000 759.375,1000 765,1000 770.625,1000 776.25,1000 781.875,1000 787.5,1000 793.125,1000 798.75,1000 804.375,1000 810,1000 815.625,1000 821.25,1000 826.875,1000 832.5,1000 838.125,1000 843.75,1000 849.375,1000 855,1000 860.625,1000 866.25,1000 871.875,1000 877.5,1000 883.125,1000 888.75,1000 894.375,1000 900,1000 905.625,1000 911.25,1000 916.875,1000 922.5,1000 928.125,1000 933.75,1000 939.375,1000 945,1000 950.625,1000 956.25,1000 961.875,1000 967.5,1000 973.125,1000 978.75,1000 984.375,1000 990,1000 995.625,1000 1001.25,1000 1006.875,1000 1012.5,1000 1018.125,1000 1023.75,1000 1029.375,1000 1035,1000 1040.625,1000 1046.25,1000 1051.875,1000 1057.5,1000 1063.125,1000 1068.75,1000 1074.375,1000 1080,1000 1085.625,1000 1091.25,1000 1096.875,1000 1102.5,1000 1108.125,1000 1113.75,1000 1119.375,1000 1125,1000 1130.625,1000 1136.25,1000 1141.875,1000 1147.5,1000 1153.125,1000 1158.75,1000 1164.375,1000 1170,1000 1175.625,1000 1181.25,1000 1186.875,1000 1192.5,1000 1198.125,1000 1203.75,1000 1209.375,1000 1215,1000 1220.625,1000 1226.25,1000 1231.875,1000 1237.5,1000 1243.125,1000 1248.75,1000 1254.375,1000 1260,1000 1265.625,1000 1271.25,1000 1276.875,1000 1282.5,1000 1288.125,1000 1293.75,1000 1299.375,1000 1305,1000 1310.625,1000 1316.25,1000 1321.875,1000 1327.5,1000 1333.125,1000 1338.75,1000 1344.375,1000 1350,1000 1355.625,1000 1361.25,1000 1366.875,1000 1372.5,1000 1378.125,1000 1383.75,1000 1389.375,1000 1395,1000 1400.625,1000 1406.25,1000 1411.875,1000 1417.5,1000 1423.125,1000 1428.75,1000 1434.375,1000 1440,1000'
	],
	path : null,
	length : null,
	container : null,
	
	init : function() {
		
		this.container = Snap('.svg-load');

		this.path = this.container.path( this.paths[0] )
					.attr({
						fill: "none",
						stroke: "#FFF",
						strokeWidth: 1
					});

		this.length = this.path.getTotalLength();

		this.path.attr({
			'stroke-dasharray' : this.length,
			'stroke-dashoffset' : this.length
		});

		EventManager.on('VENTURE_HERO_IN', $.proxy(this.pathToFlatAlps, this));
		EventManager.on('VENTURE_HERO_OUT', $.proxy(this.pathToFlat, this));
	},

	onLoad : function ( percent ) {
		
		this.path.attr({
			'stroke-dashoffset' : this.length - ( percent * this.length / 100 )
		});
	},

	pathToFlatAlps : function ( e, firstTime ) {

		var duration = 1000;
		this.path
			.attr({
				opacity: 1
			})
			.animate({
				d : this.paths[1]
			}, duration, mina.easeinout, firstTime ? ($.proxy(this.onPathMorphed, this)) : null );

		$('.svg-load').removeClass('flat');
	},

	onPathMorphed : function () {
		EventManager.trigger('VENTURE_LOAD_PATH_MORPHED');
	},

	pathToFlat : function () {

		var duration = 700;
		this.path
			.attr({
				opacity: 1
			}).animate({
				d : this.paths[2]
			}, duration, mina.easeinout, $.proxy(this.onPathFlat, this));

		$('.svg-load').addClass('flat');
	},

	onPathFlat : function() {
		this.path.attr({
			opacity: 0
		});
	}
};


var ScrollLine = window.ScrollLine || {

	$scrollTarget : null,

	init : function() {
		
		this.$scrollTarget = $('.scroll-percentage');
	},

	update : function ( ch, st, sh ) {

		if ( st < ch ) {
			this.$scrollTarget.css('width', '0%' );
			return;
		}

		var scrollPercentage = 100 * (st - ch) / (sh - ch - ch);
		this.$scrollTarget.css('width', scrollPercentage + '%' );
	}
};

var Slider = window.Slider || {

	currentSlick : null,

	init : function() {

		EventManager.on('VENTURE_UPDATE_SHOE', $.proxy( this.resetSlick, this ));
		this.initSlick();
	},

	resetSlick : function () {
		
		this.destroySlick();
		this.initSlick();
	},

	initSlick : function() {

		this.currentSlick = $('.visible .slider-tooltips');
		this.currentSlick.slick({
			dots : true
		});
	},

	destroySlick : function () {

		if ( this.currentSlick ) this.currentSlick.slick('unslick');
	}
};
var TrailMap = window.TrailMap || {

	red: '#d75e5e',
	grey: '#777777',

	$dom : null,
	topTrigger : null,

	trailPath : null,
	overlayPath : null,
	maxLen : null,
	maxMiles : null,
	noScroll : null,

	endMarker : null,
	endIcon : null,
	endIconRed : null,

	trailCoordinates : [],
	isMapReady : false,

	init : function () {

		this.$dom = $('.block-trail-map__map');
		this.$domContainer = $('.block-trail-map__map__container');
		this.maxMiles = $('.block-trail-map__map .mile').length;

		this.noScroll = this.$dom.hasClass('no-scroll');
		
		this.resize();
	},

	initTouch : function() {

		this.$dom = $('.block-trail-map__map');
		this.$domContainer = $('.block-trail-map__map__container');

		this.noScroll = this.$dom.hasClass('no-scroll');
		this.$dom.addClass('has-touch');
	},

	initMap : function () {

		var styles = [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}];

		this.map = new google.maps.Map( $('.block-trail-map__map__path')[0], {
			center: {
				lat: 47.249385, 
				lng: 9.3432343
			},
			scrollwheel: false,
			draggable: false,
			zoom: 6,
			disableDefaultUI: true,
			mapTypeId: google.maps.MapTypeId.TERRAIN,
			styles: styles
		});

		this.resize();
		this.getGPX();
	},

	getGPX : function () {

		console.log( 'get gpx', this.$dom.data('file') );

		$.get({
			url : '../../data/trails/' + this.$dom.data('file'),
			success : $.proxy(this.onGPXLoaded, this)
		});
	},

	onGPXLoaded : function ( gpx ) {

		console.log( 'gpx loaded', gpx );

		// BUILD Path
		// ZOOM Map to bounds

		var 
			$point,
			pointCoords, lat, lng,
			self = this,
			bounds = new google.maps.LatLngBounds();

		$(gpx).find('trk').find('trkpt').each( function( i, point ){

			$point = $(point);
			lat = parseFloat($point.attr('lat'));
			lng = parseFloat($point.attr('lon'));

			pointCoords = {
				lat : lat,
				lng : lng
			};
			self.trailCoordinates.push( pointCoords );

			bounds.extend( new google.maps.LatLng(lat, lng) );
		} );

		this.trailPath = new google.maps.Polyline({
			path: self.trailCoordinates,
			geodesic: true,
			strokeColor: this.grey,
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		this.maxLen = this.trailPath.getPath().length;

		this.overlayPath = new google.maps.Polyline({
			geodesic: true,
			strokeColor: this.red,
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		this.trailPath.setMap(this.map);
		this.overlayPath.setMap(this.map);

		this.map.fitBounds( bounds );

		this.isMapReady = true;

		this.addIcons();
		this.resize();

		if ( this.noScroll ) {
			this.overlayPath.setPath( this.trailCoordinates );
		}
	},

	addIcons : function () {
		
		var startIcon = {
			url: '../assets/img/skin/icon-start.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		var startMarker = new google.maps.Marker({
			position: {
				lat: this.trailCoordinates[0].lat,
				lng: this.trailCoordinates[0].lng
			},
			map: this.map,
			icon: startIcon
		});

		this.endIcon = {
			url: '../assets/img/skin/icon-end.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		this.endIconRed = {
			url: '../assets/img/skin/icon-end-red.png',
			size: new google.maps.Size(284, 245),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(71, 61),
			scaledSize: new google.maps.Size(142, 122)
		};

		var trailLenght = this.trailCoordinates.length;
		this.endMarker = new google.maps.Marker({
			position: {
				lat: this.trailCoordinates[trailLenght-1].lat,
				lng: this.trailCoordinates[trailLenght-1].lng
			},
			map: this.map,
			// icon: this.endIcon, 
			icon: this.endIconRed
		});

	},

	drawPath : function ( length ) {

		var visibleCoordinates = this.trailCoordinates.slice( 0, length );
		this.overlayPath.setPath( visibleCoordinates );

		if ( length >= this.trailCoordinates.length - 10 ) {
			this.endMarker.setIcon( this.endIconRed );
		} else {
			this.endMarker.setIcon( this.endIcon );
		}
	},

	updateMiles : function ( mile ) {

		$('.mile.visible').removeClass('visible');
		for ( var i = 0; i < mile; i++ ) {
			$('.mile:eq('+i+')').addClass('visible');
		}
	},

	update : function ( ch, st ) {

		var 
			mapHeight = 3000,
			maxScrollTop = this.topTrigger + mapHeight - ch;

		if ( st > this.topTrigger && st < maxScrollTop ) {

			this.$domContainer
				.removeClass('is-bottomed')
				.addClass('is-fixed');

			if ( this.isMapReady ) {
				this.drawPath( Math.ceil(this.maxLen * ( st - this.topTrigger ) / (mapHeight - ch - 100)) );
				this.updateMiles( Math.ceil(this.maxMiles * ( st - this.topTrigger ) / (mapHeight - ch - 100)) );
			}

		} else if ( st >= maxScrollTop ) {

			this.$domContainer
				.removeClass('is-fixed')
				.addClass('is-bottomed');

			if ( this.isMapReady ) this.overlayPath.setPath( this.trailCoordinates );

		} else {

			this.$domContainer
				.removeClass('is-bottomed')
				.removeClass('is-fixed');

			if ( this.isMapReady ) {
				this.overlayPath.setPath( [] );
				$('.mile.visible').removeClass('visible');
			}
		}
	},

	resize : function() {

		var 
			windowH = $(window).innerHeight(),
			windowW = $(window).innerWidth();

		if ( this.$domContainer ) this.$domContainer.css('height', windowH);
		if ( this.$dom ) this.topTrigger = this.$dom.position().top;

		if ( windowW > 690 && windowW < 1024 && $('.block-trail-map__map').hasClass('has-touch') ) {
			$('.block-trail-map__map__miles__scroller__inside').css('width', $('.block-trail-map__map .mile').length * 142);
		}
	}
};

var TrailWaypoints = window.TrailWaypoints || {

	elHero : null,
	elBackground : null,
	activeHero : null,

	init : function() {

		var self = this;
		
		self.elHero = $('.block-trail-hero');
		self.elBackground = $('.block-trail-hero__background');

		Waypoint.destroyAll();

		var inviewHero = new Waypoint.Inview({
			element: self.elHero,
			enter: function() {
				self.activeHero = true;
			},
			exited: function() {
				self.activeHero = false;
			}
		});
	},

	update : function ( clientHeight, scrollTop, scrollHeight ) {

		if ( !this.activeHero ) return;
		this.updateHero( scrollTop, clientHeight );
	},

	updateHero : function ( scrollTop, clientHeight ) {

		var translateValue = Utils.map( scrollTop, 0, clientHeight, 0, (0.2*clientHeight) );
		this.elBackground.css('transform', 'translateY('+translateValue+'px)');
	}
};

var Home = window.Home || {

	$scrollOrigin : null,
	isTouchDevice : null,
	isFlat : false,

	init : function() {

		this.isTouchDevice = Utils.is_touch_device() || Utils.isMSIE();
		
		this.$scrollOrigin = Utils.isFirefox() ? $('html')[0] : $('body')[0];
		this.$scrollTarget = $('.scroll-percentage');

		EventManager.on('VENTURE_LOADING', $.proxy(this.onLoading, this));
		EventManager.on('VENTURE_LOADED', $.proxy(this.onLoadComplete, this));
		EventManager.on('VENTURE_LAZY_LOADED', $.proxy(this.onLazyLoadComplete, this));
		EventManager.on('VENTURE_LOAD_PATH_MORPHED', $.proxy(this.onLoadPathMorphed, this));
		
		EventManager.on('VENTURE_SHOE_CHOSEN', $.proxy(this.onShoeChosen, this));
		EventManager.on('VENTURE_UPDATE_SHOE', $.proxy(this.onShoeUpdate, this));
		
		SVGPath.init();
		Loader.init();
	},

	onLoading : function( e, percent ) {
		SVGPath.onLoad( percent );
	},

	onLoadComplete : function() {

		console.log("on load complete");
		
		if ( !this.isTouchDevice ) {
			SVGLines.init();
			RainAnimation.init();
		}
		ChoiceHero.init();
		MiniChoice.init();
		
		$('body').addClass('template__choice');
		EventManager.trigger('VENTURE_HERO_IN', true);
	},

	onLazyLoadComplete : function(){
		
		// Set loaded in PHP Session
		$.ajax({ url : 'set_loaded.php' });
	},

	onShoeChosen : function() {

		this.initScroll();
		if ( !this.isTouchDevice ) HomeWaypoints.init();
		else this.updateShoe();
	},

	onShoeUpdate : function ( e, model ) {

		if ( !this.isTouchDevice ) HomeWaypoints.init();
		else this.updateShoe();
	},

	initScroll : function () {

		ScrollLine.init();
		$(document).on('scroll', Utils.debounce( $.proxy(this.onScroll, this), 8 ));
		$(window).on('resize', Utils.debounce( $.proxy(this.onResize, this), 100 ));
	},

	onResize : function () {

		this.repositionTooltip();
	},

	onScroll : function () {

		var ch = this.$scrollOrigin.clientHeight;
		var st = this.$scrollOrigin.scrollTop;
		var sh = this.$scrollOrigin.scrollHeight;

		MiniChoice.update( ch, st );
		ScrollLine.update( ch, st, sh );
		if ( !this.isTouchDevice ) HomeWaypoints.update( ch, st, sh );
	},

	onLoadPathMorphed : function () {

		var self = this;
		setTimeout( function () {
			ChoiceHero.initShoe();
			Slider.init();
			self.repositionTooltip();
		}, 1000 );
	},

	updateShoe : function () {
		
		// Set all feature to image max
		var model = $('body').data('model');
		$('body').addClass('has-touch');
		$('.block-feature.visible').each( function( i, feature ) {
			var 
				$feature = $(feature);

			$feature.find('img').attr('src', '/assets/img/' + model + '/' + model + '-sequence-' + (i+1) + '-' + $feature.data('max') + '.png' );
		} );

		// Reposition tooltips
		this.repositionTooltip();
	},

	repositionTooltip : function () {

		$('.block-feature__tooltip').each( function (i, tooltip) {

			var $tooltip = $(tooltip),
				$parent = $tooltip.parent(),
				parentWidth = $parent.width(),
				parentHeight = $parent.height(),
				refWidth = 880,
				refHeight = 422,
				top = parseInt($tooltip.data('top')),
				left = parseInt($tooltip.data('left')),
				newTop = top,
				newLeft = left;

			if ( $(window).innerWidth() < 880 ) {
				newTop = ( (parentHeight*top)/refHeight );
				newLeft = ( (parentWidth*left)/refWidth );
			}

			$tooltip.css({
				"top" : newTop,
				"left" : newLeft
			});

		} );
	}
};



var Trail = window.Trail || {

	$scrollOrigin : null,
	isTouchDevice : null,
	scrollMap : false,

	init : function() {

		this.isTouchDevice = Utils.is_touch_device() || Utils.isMSIE();
		this.initScroll();
		this.initLoad();

		if ( !this.isTouchDevice ) {
			TrailWaypoints.init();
			TrailMap.init();
		} else {
			TrailMap.initTouch();
		}
	},

	initLoad : function () {

		$('img[data-src]').each(function(i, src){
			$(src).attr('src', $(src).data('src'));
		});

		$('div[data-background], a[data-background]').each(function(i, src){
			$(src).css('background-image', 'url('+$(src).data('background')+')');
		});
	},

	initScroll : function () {

		this.scrollMap = !$('.block-trail-map__map').hasClass('no-scroll');

		this.$scrollOrigin = Utils.isFirefox() ? $('html')[0] : $('body')[0];
		$(document).on('scroll', Utils.debounce( $.proxy(this.onScroll, this), 8 ));
		$(window).on('resize', Utils.debounce( $.proxy(this.onResize, this), 10 ));
	},

	onScroll : function () {

		var ch = this.$scrollOrigin.clientHeight;
		var st = this.$scrollOrigin.scrollTop;
		var sh = this.$scrollOrigin.scrollHeight;

		if ( !this.isTouchDevice ) {
			TrailWaypoints.update( ch, st, sh );
			if ( this.scrollMap ) TrailMap.update( ch, st );
		}
	},

	onResize : function(){

		TrailMap.resize();
	}
};